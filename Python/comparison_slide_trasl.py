#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt 
import numpy as np
import json 
import math
from subprocess import call

steps 		= [50, 50, 50, 200, 200, 200]				# [mm]
# datasets 	= [1,1,1,1,1,1]
name_txt 	= ('Kneip_50.txt', 'Gao_50.txt', 'Epnp_50.txt', 'Kneip_200.txt', 'Gao_200.txt', 'Epnp_200.txt')

cases 		= [1,1,1,1, 1,1]

axis 			= 2;	# 0-x, 1-y, 2-z
if axis==0:
	axis_name 	= "x"
elif axis==1:
	axis_name 	= "y"
else:
	axis_name	= "z"

colors 		= [[85,98,112], [78,205,196], [199,244,100],
		 			[204,153,255], [255,107,107], [196,77,88]]


path 			= "../RESULTS/Slide/"

dataset 		= 1

camera1 		= np.matrix([0, 0, 0, 1]).T
camera1_set 	= np.matrix([0, 0, 0, 1]).T
expected_point 	= np.matrix([0, 0, 0, 1]).T
error 			= np.matrix([0, 0, 0]).T
tot_abs_error 	= np.matrix([0, 0, 0]).T


colors 			= map(lambda x:np.array(x)/255., colors)
markers 		= ('o', '>', 's', 'v', 'D', 'H')
mar_size		= 8
legend_names 	= ('Kneip - passo 50 mm', 
					'Gao - passo 50 mm',
					'EPnP - passo 50 mm',
					'Kneip - passo 200 mm',
					'Gao - passo 200 mm',
					'EPnP - passo 200 mm')

errors 				= []
z_errors_tot 		= []
draws_error_tot 	= []
abs_errors 			= []
z_abs_errors_tot 	= []
draws_abs_error_tot = []
percent_errors		= []
z_percent_errors 	= []
draws_percent		= []
z_percent_tot 		= []
draws_percent_tot 	= []

for i in xrange(len(name_txt)):

	if cases[i]:

		# read the transformation matrix
		in_file = open(path + name_txt[i],"r")
		text = in_file.read()
		in_file.close()
		dati = json.loads(text.replace('{','[').replace('}',']'))
		for d in xrange(len(dati)): 
			dati[d] = dati[d][0]

		# at each time...
		for d in dati:

			# expected point after the z axis translation
			new_expected_point = expected_point[0:3] + np.matrix([0, 0, steps[i]]).T

			# create the roto-translation 4x4 matrix 
			d.append([0, 0, 0, 1])
			Rt = np.matrix(d)

			# apply the transformation to the left camera center
			new_camera1 = Rt*camera1
			new_camera1_set = Rt*camera1_set

			# calculate the translation error
			new_error = new_camera1[0:3] - new_expected_point
			errors.append(new_error)

			# % error
			percent = abs(new_error[axis])/new_expected_point[2]*100;
			percent_errors.append(percent)

			# calculate the cumulative translation error
			expected_step = np.matrix([0, 0, steps[i]]).T
			new_abs_error = new_camera1_set[0:3] - expected_step
			new_tot_abs_error = abs(tot_abs_error) + abs(new_abs_error)
			abs_errors.append(new_tot_abs_error)

			# redefine the variables
			error = new_error
			camera1 = new_camera1
			expected_point = new_expected_point
			tot_abs_error = new_tot_abs_error

		# define a vector that contains the errors at each step
		z_errors = [0]
		for e in errors:
			z_errors.append(float(e[axis]))
		errors = []
		z_errors_tot.append(z_errors)


		z_percent_errors = [0]
		for e in percent_errors:
			z_percent_errors.append(float(e))
		percent_errors = []
		z_percent_tot.append(z_percent_errors)

		z_abs_errors = [0]
		for e in abs_errors:
			z_abs_errors.append(float(e[axis]))
		abs_errors = []
		z_abs_errors_tot.append(z_abs_errors)

	# repeat the cicle for each algorithm considered 
	camera1 = np.matrix([0, 0, 0, 1]).T
	expected_point = np.matrix([0, 0, 0, 1]).T
	error = np.matrix([0, 0, 0]).T
	tot_abs_error = np.matrix([0, 0, 0]).T

# 0 Kneip 50, 1 Gao 50, 2 Epnp 50, 3 Kneip 200, 4 Gao 200, 5 Epnp 200
case = 5		
print z_abs_errors_tot[case]
# print z_abs_errors_tot[case][-1]
# print len(z_abs_errors_tot[case][1:])
mean_error = (z_abs_errors_tot[case][-1])/len(z_abs_errors_tot[case][1:])
percent = mean_error/steps[case]*100
print mean_error
print percent

############## figure 1: real error vs real z axis translation ################

fig = plt.figure()
ax = fig.add_subplot(111)


draws = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws)):
	if dataset==1:
		path_length 	= 1350			
	else:
		path_length 	= 1340
	x = np.array(xrange(int(path_length/steps[j])+1))*steps[j]
	# frame = [steps[j]*i for i in xrange(path_length/steps[j]+1)]
	draws[j] = ax.plot(x, z_errors_tot[j], linewidth=2,marker = markers[indices[j]],
			 	markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 	color = colors[indices[j]])
	draws_error_tot.append(draws[j])
	

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

# ax.legend(map(lambda x:x[0],draws_error_tot), names,
# 		loc='upper center', bbox_to_anchor=(0.5, -0.12),
#         fancybox=True, shadow=False, ncol=2)

# ax.set_title('Translation test No.1', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel(axis_name+'-axis translation error [mm]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_z/error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/error.pdf'])

################### figure 2: % error vs real z axis translation #####################

fig = plt.figure()
ax = fig.add_subplot(111)

draws_percent = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws)):
	if dataset==1:
		path_length 	= 1350			
	else:
		path_length 	= 1340
	x = np.array(xrange(int(path_length/steps[j])+1))*steps[j]
	draws_percent[j] = ax.plot(x, z_percent_tot[j], linewidth=2,marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_percent_tot.append(draws[j])
	
	

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.5, box.height * 0.5])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc=2, bbox_to_anchor=(1,1),
        fancybox=True, shadow=False, ncol=1)

# ax.set_title('Translation test No.1', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel(axis_name+'-axis translation error [%]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_z/percent_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/percent_error.pdf'])

####################### figure 3: cumulative error vs frame ########################

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)


draws_abs = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws_abs)):
	if dataset==1:
		path_length 	= 1350			
	else:
		path_length 	= 1340
	x = np.array(xrange(int(path_length/steps[j])+1))*steps[j]
	draws_abs[j] = ax.plot(x, z_abs_errors_tot[j],linewidth=2, marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_abs_error_tot.append(draws_abs[j])
	

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

# ax.legend(map(lambda x:x[0],draws_error_tot), names,
# 		loc='upper center', bbox_to_anchor=(0.5, -0.12),
#         fancybox=True, shadow=False, ncol=2)

# ax.set_title('Translation test No.1', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel(axis_name+'-axis cumulative error [mm]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_z/cumulative_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/cumulative_error.pdf'])


plt.show()
