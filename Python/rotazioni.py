#!/usr/bin/python
# Filename: rotazioni.py

import math
import numpy as np

def Eul_2_R(Eulero):
	c1=math.cos(Eulero[0])
	s1=math.sin(Eulero[0])
	c2=math.cos(Eulero[1])
	s2=math.sin(Eulero[1])
	c3=math.cos(Eulero[2])
	s3=math.sin(Eulero[2])
	R=[[c1*c2, c1*s2*s3-c3*s1, s1*s3+c1*c3*s2], [c2*s1, c1*c3+s1*s2*s3, c3*s1*s2-c1*s3], [-s2, c2*s3, c2*c3]]
	return R

def R_2_Eul(R):
	Eul = np.matlib.zeros((3,1))
	Eul[0] = math.atan2(R[1,0],R[0,0]);
	Eul[1] = (math.atan2(-R[2,0], math.sqrt(math.pow(R[0,0],2) + 
				math.pow(R[1,0],2)))+ math.atan2(-R[2,0], 
				math.sqrt(math.pow(R[2,1],2)+math.pow(R[2,2],2))))/2;
	Eul[2] = math.atan2(R[2,1],R[2,2]);
	return Eul

# End of rotazioni.py