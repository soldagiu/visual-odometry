import matplotlib.pyplot as plt 
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import numpy.matlib
import json 
import math
import itertools
from pylab import *
from subprocess import call

name_txt = ''

# define the function blocks
def Ransac_Kneip():
	global name_txt
	name_txt = 'RtRansacKneip.txt'

def Ransac_Gao():
	global name_txt
	name_txt = 'RtRansacGao.txt'

def Ransac_Epnp():
	global name_txt
	name_txt = 'RtRansacEpnp.txt'

def NonLin_Kneip():
	global name_txt
	name_txt = 'RtNonLinKneip.txt'

def NonLin_Gao():
	global name_txt
	name_txt = 'RtNonLinGao.txt'

def NonLin_Epnp():
	global name_txt
	name_txt = 'RtNonLinEpnp.txt'

# map the inputs to the function blocks
case = {1 : Ransac_Kneip,
        2 : Ransac_Gao,
        3 : Ransac_Epnp,
        4 : NonLin_Kneip,
        5 : NonLin_Gao,
        6 : NonLin_Epnp,
}

case[5]()

step 			= 100				# [mm]
step_deg		= 5					# [deg]
# path 			= '../RESULTS/METHODS/Translation1_'+str(step)+'mm/'
path 			= '../RESULTS/METHODS/Rotation1_'+str(step_deg)+'deg/'

# transformation between camera 1 and camera 2
Eul12 	= [0.02, 0.1, 0.4]			# deg
Eul12 	= map(math.radians, Eul12)	# rad
c1 		= math.cos(Eul12[0])
s1 		= math.sin(Eul12[0])
c2 		= math.cos(Eul12[1])
s2 		= math.sin(Eul12[1])
c3 		= math.cos(Eul12[2])
s3 		= math.sin(Eul12[2])
rot12 	= np.matrix([[c1*c2, c1*s2*s3-c3*s1, s1*s3+c1*c3*s2],
         [c2*s1, c1*c3+s1*s2*s3, c3*s1*s2-c1*s3],
         [-s2, c2*s3, c2*c3]])
trasl12 = np.matrix([546, -1.5, -3.5]).T;		# [mm]
Rt12 	= np.concatenate([rot12, trasl12], axis=1)
lastrow	= np.matrix([0,0,0,1])
Rt12 	= np.concatenate([Rt12, lastrow], axis=0)

camera1 = np.matrix([0,0,0,1]).T
camera2 = Rt12*camera1
centro = (camera1[np.ix_([0,1,2],[0])]+camera2[np.ix_([0,1,2],[0])])/2

positions_cam1 = []
positions_cam2 = []
positions_centro = []

positions_cam1.append(camera1)
positions_cam2.append(camera2)
positions_centro.append(centro)



# read the transformation matrix
in_file = open(path + name_txt, "r")
text = in_file.read()
in_file.close()
dati = json.loads(text.replace('{','[').replace('}',']'))
for d in xrange(len(dati)): 
	dati[d] = dati[d][0]

# maxRt = np.eye(4)

# at each time...
for d in dati:

	# create the roto-translation 4x4 matrix 
	d.append([0, 0, 0, 1])
	Rt = np.matrix(d)

	# maxRt = Rt*maxRt
	camera1 = camera1*Rt
	# camera2 = maxRt*Rt12*camera1
	camera2 = camera1*Rt12
	centro = (camera1[np.ix_([0,1,2],[0])]+camera2[np.ix_([0,1,2],[0])])/2
	

	positions_cam1.append(camera1)
	positions_cam2.append(camera2)
	positions_centro.append(centro)



x1 = []
y1 = []
z1 = []

for i in positions_cam1:
	x1.append(float(i[0]))
	y1.append(float(i[1]))
	z1.append(float(i[2]))
positions_cam1 = []


x2 = []
y2 = []
z2 = []

for i in positions_cam2:
	x2.append(float(i[0]))
	y2.append(float(i[1]))
	z2.append(float(i[2]))
positions_cam2 = []

x0 = []
y0 = []
z0 = []

for i in positions_centro:
	x0.append(float(i[0]))
	y0.append(float(i[1]))
	z0.append(float(i[2]))
positions_centro = []

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')

# x = np.arange(len(x1))
# ys = [i+x+(i*x)**2 for i in range(len(x1))]
# colors = iter(cm.rainbow(np.linspace(0, 1, len(ys))))
# for y in xrange(len(ys)):
# 	col = next(colors)
# 	ax.plot([x1[y], x2[y]], [y1[y],y2[y]],zs=[z1[y],z2[y]], color=col)
# 	draw_camera1 = ax.scatter(x1[y],y1[y],z1[y], s=50, color=col, marker='^')
# 	draw_camera2 = ax.scatter(x2[y],y2[y],z2[y], s=50, color=col, marker='^')
	

# # Create cubic bounding box to simulate equal aspect ratio
# max_range = max([max(x1)-min(x1), max(y1)-min(y1), max(z1)-min(z1), 
# 				max(x2)-min(x2), max(y2)-min(y2), max(z2)-min(z2)])
# Xb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][0].flatten() + 0.5*(max(x2)+min(x1))
# Yb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][1].flatten() + 0.5*(max(y2)+min(y1))
# Zb = 0.5*max_range*np.mgrid[-1:2:2,-1:2:2,-1:2:2][2].flatten() + 0.5*(max(z2)+min(z1))
# for xb, yb, zb in zip(Xb, Yb, Zb):
#    ax.plot([xb], [yb], [zb], 'w')


# ax.set_title('3D camera path'+' - Rotation test No.1')
# ax.set_xlabel('[mm]')
# ax.set_ylabel('[mm]')
# ax.set_zlabel('[mm]')

# ax.xaxis._axinfo['label']['space_factor'] = 2
# ax.yaxis._axinfo['label']['space_factor'] = 2
# ax.zaxis._axinfo['label']['space_factor'] = 2

# plt.savefig(path+'camera_paths/3D.pdf', format='pdf', dpi=1000)
# call(['pdfcrop', '--margins', '10', path+'camera_paths/3D.pdf'])

# piano x-z

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)

x = np.arange(len(x1))
ys = [i+x+(i*x)**2 for i in range(len(x1))]
colors1 = iter(cm.Reds(np.linspace(0.5, 1, len(ys))))
colors2 = iter(cm.Blues(np.linspace(0.5, 1, len(ys))))
colors3 = iter(cm.Greys(np.linspace(0.5, 1, len(ys))))
for j in xrange(len(x1)):
	col1 = next(colors1)
	col2 = next(colors2)
	col3 = next(colors3)
	ax.plot([x1[j], x2[j]], [z1[j],z2[j]], color=col3, linestyle='--')
	camera1 = ax.plot(x1[j], z1[j], marker='^', markeredgecolor=col1, markersize=10, color=col1)
	camera2 = ax.plot(x2[j], z2[j], marker='^', markeredgecolor=col2, markersize=10, color=col2)
	# centro = ax.plot(x0[j], z0[j], marker='^', markeredgecolor=col3, markersize=10, color=col3)
	
axes().set_aspect('equal', 'datalim')
ax.set_title('Camera path - Rotation test No.1', fontsize = 14)
plt.xlabel('x [mm]', fontsize = 15)
plt.ylabel('z [mm]', fontsize = 15)
plt.grid()

plt.savefig(path+'camera_paths/xz.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'camera_paths/xz.pdf'])


# # piano x-y

# fig_abs = plt.figure()
# ax = fig_abs.add_subplot(111)
# colors1 = iter(cm.Reds(np.linspace(0, 1, len(ys))))
# colors2 = iter(cm.Blues(np.linspace(0, 1, len(ys))))
# colors3 = iter(cm.Greys(np.linspace(0, 1, len(ys))))
# ys = [i+x+(i*x)**2 for i in range(len(x1))]
# for j in xrange(len(x1)):
# 	col1 = next(colors1)
# 	col2 = next(colors2)
# 	col3 = next(colors3)
# 	ax.plot([x1[j], x2[j]], [y1[j],y2[j]], color=col3, linestyle='--')
# 	camera1 = ax.plot(x1[j], y1[j], marker='^', markeredgecolor=col1, markersize=10, color=col1)
# 	camera2 = ax.plot(x2[j], y2[j], marker='^', markeredgecolor=col2, markersize=10, color=col2)


# axes().set_aspect('equal', 'datalim')
# ax.set_title('Camera path - Rotation test No.1', fontsize = 14)
# plt.xlabel('x [mm]', fontsize = 15)
# plt.ylabel('y [mm]', fontsize = 15)
# plt.grid()

# plt.savefig(path+'camera_paths/xy.pdf', format='pdf', dpi=1000)
# call(['pdfcrop', '--margins', '10', path+'camera_paths/xy.pdf'])

# # piano y-z

# fig_abs = plt.figure()
# ax = fig_abs.add_subplot(111)
# colors1 = iter(cm.Reds(np.linspace(0, 1, len(ys))))
# colors2 = iter(cm.Blues(np.linspace(0, 1, len(ys))))
# colors3 = iter(cm.Greys(np.linspace(0, 1, len(ys))))
# ys = [i+x+(i*x)**2 for i in range(len(x1))]
# for j in xrange(len(x1)):
# 	col1 = next(colors1)
# 	col2 = next(colors2)
# 	col3 = next(colors3)
# 	ax.plot([y1[j], y2[j]], [z1[j],z2[j]], color=col3, linestyle='--')
# 	camera1 = ax.plot(y1[j], z1[j], marker='^', markeredgecolor=col1, markersize=10, color=col1)
# 	camera2 = ax.plot(y2[j], z2[j], marker='^', markeredgecolor=col2, markersize=10, color=col2)


# axes().set_aspect('equal', 'datalim')
# ax.set_title('Camera path - Rotation test No.1', fontsize = 14)
# plt.xlabel('y [mm]', fontsize = 15)
# plt.ylabel('z [mm]', fontsize = 15)
# plt.grid()

plt.savefig(path+'camera_paths/yz.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'camera_paths/yz.pdf'])

plt.show()




