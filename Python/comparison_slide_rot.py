#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt 
import numpy as np
import numpy.matlib
import json 
import math
from subprocess import call

name_txt 	= ('Kneip_5_deg.txt', 'Gao_5_deg.txt', 'Epnp_5_deg.txt', 'Kneip_15_deg.txt', 'Gao_15_deg.txt', 'Epnp_15_deg.txt')

cases 			= [1,1,1,1,1,1]

dataset 		= "1"

step_deg 		= [5,5,5,15,15,15]			
step 			= map(lambda x:math.radians(x),step_deg)		
path 			= "../RESULTS/Slide/"
path_length 	= 90				# [deg]
# num_el 			= path_length/step_deg;
# frame 			= np.linspace(0,num_el,num_el+1)

camera1 		= np.matrix([0, 0, 0]).T
camera1_set 	= np.matrix([0, 0, 0]).T
expected_point 	= np.matrix([0, 0, 0]).T

error 			= np.matrix([0, 0, 0]).T
tot_abs_error 	= np.matrix([0, 0, 0]).T

colors 	= [[180,222,193], [92,88,99], [255,31,76],
 		 		[168,81,99], [63,184,175],[100,54,255]]
colors 			= map(lambda x:np.array(x)/255., colors)		
markers 		= ('s', 'o', 'v', 'D', 'H', '>')
mar_size		= 8
legend_names 	= ('Kneip - passo 5 deg', 
					'Gao - passo 5 deg',
					'EPnP - passo 5 deg',
					'Kneip - passo 15 deg',
					'Gao - passo 15 deg',
					'EPnP - passo 15 deg')

errors 				= []
y_errors_tot 		= []
draws_error_tot 	= []
abs_errors 			= []
y_abs_errors_tot 	= []
draws_abs_error_tot = []
percent_errors		= []
y_percent_errors 	= []
draws_percent		= []
y_percent_tot 		= []
draws_percent_tot 	= []

for i in xrange(len(name_txt)):

	

	if cases[i]:

		# read the transformation matrix
		in_file = open(path + name_txt[i],"r")
		text = in_file.read()
		in_file.close()
		dati = json.loads(text.replace('{','[').replace('}',']'))
		for d in xrange(len(dati)): 
			dati[d] = dati[d][0]

		# at each time...
		for d in dati:

			# expected point after the y axis rotation
			new_expected_point = expected_point + np.matrix([0, step[i], 0]).T

			# create the roto-translation 4x4 matrix 
			Rt = np.matrix(d)
			R = Rt[0:3,0:3]

			Eul = np.matlib.zeros((3,1))
			 
			Eul[0] = math.atan2(R[1,0],R[0,0]);
			Eul[1] = (math.atan2(-R[2,0], math.sqrt(math.pow(R[0,0],2) + 
					math.pow(R[1,0],2)))+ math.atan2(-R[2,0], 
					math.sqrt(math.pow(R[2,1],2)+math.pow(R[2,2],2))))/2;
			Eul[2] = math.atan2(R[2,1],R[2,2]);
			
			# apply the transformation to the left camera center
			new_camera1 = camera1 + Eul
			new_camera1_set = camera1_set + Eul

			# calculate the rotation error
			new_error = new_camera1[0:3] - new_expected_point
			errors.append(new_error)

			# % error
			percent = abs(new_error[1])/new_expected_point[1]*100;
			percent_errors.append(percent)

			# calculate the cumulative rotation error
			expected_step 	= np.matrix([0, step[i], 0]).T
			new_abs_error = new_camera1_set[0:3] - expected_step
			new_tot_abs_error = abs(tot_abs_error) + abs(new_abs_error)
			# print new_tot_abs_error[1]
			abs_errors.append(new_tot_abs_error)
			
			# redefine the variables
			error = new_error
			camera1 = new_camera1
			expected_point = new_expected_point
			tot_abs_error = new_tot_abs_error


		# define a vector that contains the errors at each step
		y_errors = [0]
		for e in errors:
			y_errors.append(math.degrees(float(e[1])))
		errors = []
		y_errors_tot.append(y_errors)

		y_percent_errors = [0]
		for e in percent_errors:
			y_percent_errors.append(float(e))
		percent_errors = []
		y_percent_tot.append(y_percent_errors)

		y_abs_errors = [0]
		for e in abs_errors:
			y_abs_errors.append(math.degrees(float(e[1])))
		abs_errors = []
		y_abs_errors_tot.append(y_abs_errors)

	# repeat the cicle for each algorithm considered 
	camera1 = np.matrix([0, 0, 0]).T
	expected_point = np.matrix([0, 0, 0]).T
	error = np.matrix([0, 0, 0]).T
	tot_abs_error = np.matrix([0, 0, 0]).T

# 0 Kneip 5, 1 Gao 5, 2 Epnp 5, 3 Kneip 15, 4 Gao 15, 5 Epnp 15
case =5	
print y_abs_errors_tot[case]
# print z_abs_errors_tot[case][-1]
# print len(z_abs_errors_tot[case][1:])
mean_error = (y_abs_errors_tot[case][-1])/len(y_abs_errors_tot[case][1:])
percent = mean_error/step_deg[case]*100
print mean_error
print percent


############## figure 1: real error vs real z axis translation ##########################

fig = plt.figure()
ax = fig.add_subplot(111)

draws = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws)):
	x = np.array(xrange(int(path_length/step_deg[j])+1))*step_deg[j]
	draws[j] = ax.plot(x, y_errors_tot[j],linewidth=2, marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_error_tot.append(draws[j])
	

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

# ax.legend(map(lambda x:x[0],draws_error_tot), names,
# 		loc='upper center', bbox_to_anchor=(0.5, -0.15),
#         fancybox=True, shadow=False, ncol=2)

# ax.set_title('Rotation test No.'+dataset+'\nstep size '+ str(step_deg)+' deg', fontsize=14)
plt.xlabel('y-axis rotation [deg]', fontsize = 15)
plt.ylabel('y-axis error [deg]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_y/error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_y/error.pdf'])

################## figure 2: % error vs real z axis translation ##################################

fig = plt.figure()
ax = fig.add_subplot(111)


draws_percent = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws)):
	x = np.array(xrange(int(path_length/step_deg[j])+1))*step_deg[j]
	draws_percent[j] = ax.plot(x, y_percent_tot[j],linewidth=2, marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_percent_tot.append(draws[j])
	

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.5, box.height * 0.5])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc=2, bbox_to_anchor=(1,1),
        fancybox=True, shadow=False, ncol=1)

# ax.set_title('Rotation test No.'+dataset+'\nstep size '+ str(step_deg)+' deg', fontsize=14)
plt.xlabel('y-axis rotation [deg]', fontsize = 15)
plt.ylabel('y-axis rotation error [%]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_y/percent_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_y/percent_error.pdf'])

################ figure 3: cumulative error vs frame ##########################

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)


draws_abs = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws_abs)):
	x = np.array(xrange(int(path_length/step_deg[j])+1))*step_deg[j]
	draws_abs[j] = ax.plot(x, y_abs_errors_tot[j],linewidth=2, marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_abs_error_tot.append(draws_abs[j])
	


box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

# ax.legend(map(lambda x:x[0],draws_error_tot), names,
# 		loc='upper center', bbox_to_anchor=(0.5, -0.15),
#         fancybox=True, shadow=False, ncol=2)

# ax.set_title('Rotation test No.'+dataset+'\nstep size '+ str(step_deg)+' deg', fontsize=14)
plt.xlabel('y-axis rotation [deg]', fontsize = 15)
plt.ylabel('y-axis cumulative error [deg]', fontsize = 15)
plt.grid()
plt.savefig(path+'Graphs_y/cumulative_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_y/cumulative_error.pdf'])

plt.show()

