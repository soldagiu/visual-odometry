import matplotlib.pyplot as plt 
import numpy as np
import json 
import math

from subprocess import call

name_txt = 'Rt.txt'

step 			= 200				# [mm]
path 			= "../RESULTS/Axis_trasl/"
path_length 	= 1350				# [mm]

camera1 		= np.matrix([0, 0, 0, 1]).T
camera1_set 	= np.matrix([0, 0, 0, 1]).T
expected_point 	= np.matrix([0, 0, 0, 1]).T
expected_step 	= np.matrix([0, 0, step]).T
error 			= np.matrix([0, 0, 0]).T
tot_abs_error 	= np.matrix([0, 0, 0]).T

colors 			= [[0,0,128], [255,0,128], [32,207,111]]
colors 			= map(lambda x:np.array(x)/255., colors)
markers 		= ('s', 'o', 'v', 'D', 'H', '>')
mar_size		= 5
legend_names 	= ('x', 'y', 'z')

axis = [0,1,2]

errors 				= []
z_errors_tot 		= []
draws_error_tot 	= []
abs_errors 			= []
z_abs_errors_tot 	= []
draws_abs_error_tot = []
percent_errors		= []
z_percent_errors 	= []
draws_percent		= []
z_percent_tot 		= []
draws_percent_tot 	= []

for i in axis:

	# read the transformation matrix
	in_file = open(path + name_txt,"r")
	text = in_file.read()
	in_file.close()
	dati = json.loads(text.replace('{','[').replace('}',']'))
	for d in xrange(len(dati)): 
		dati[d] = dati[d][0]

	# at each time...
	for d in dati:

		# expected point after the z axis translation
		new_expected_point = expected_point[0:3] + np.matrix([0, 0, step]).T

		# create the roto-translation 4x4 matrix 
		d.append([0, 0, 0, 1])
		Rt = np.matrix(d)

		# apply the transformation to the left camera center
		new_camera1 = Rt*camera1
		new_camera1_set = Rt*camera1_set

		# calculate the translation error
		new_error = new_camera1[0:3] - new_expected_point
		errors.append(new_error)

		# % error
		percent = abs(new_error[i])/new_expected_point[2]*100;
		percent_errors.append(percent)

		# calculate the cumulative translation error
		new_abs_error = new_camera1_set[0:3] - expected_step
		new_tot_abs_error = abs(tot_abs_error) + abs(new_abs_error)
		abs_errors.append(new_tot_abs_error)

		# redefine the variables
		error = new_error
		camera1 = new_camera1
		expected_point = new_expected_point
		tot_abs_error = new_tot_abs_error

	# define a vector that contains the errors at each step
	z_errors = [0]
	for e in errors:
		z_errors.append(float(e[i]))
	errors = []
	z_errors_tot.append(z_errors)

	z_percent_errors = [0]
	for e in percent_errors:
		z_percent_errors.append(float(e))
	percent_errors = []
	z_percent_tot.append(z_percent_errors)

	z_abs_errors = [0]
	for e in abs_errors:
		z_abs_errors.append(float(e[i]))
	abs_errors = []
	z_abs_errors_tot.append(z_abs_errors)



	# repeat the cicle for each algorithm considered 
	camera1 = np.matrix([0, 0, 0, 1]).T
	expected_point = np.matrix([0, 0, 0, 1]).T
	error = np.matrix([0, 0, 0]).T
	tot_abs_error = np.matrix([0, 0, 0]).T


############# figure 1: real error vs real z axis translation #####################

fig = plt.figure()
ax = fig.add_subplot(111)
x = np.array(xrange(int(path_length/step)+1))*step

draws = [0]*len(axis)

indices = []
names = []

for k in xrange(len(axis)):
	indices.append(k) 
	names.append(legend_names[k])

for j in xrange(len(draws)):
	draws[j] = ax.plot(x, z_errors_tot[j], marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_error_tot.append(draws[j])
	

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=3)

ax.set_title('Translation test No.1'+'\nstep size '+ str(step)+ ' mm', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel('error [mm]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs/error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs/error.pdf'])

##################### figure 2: % error vs real z axis translation ###########################

fig = plt.figure()
ax = fig.add_subplot(111)
x = np.array(xrange(int(path_length/step)+1))*step

draws_percent = [0]*len(axis)

indices = []
names = []

for k in xrange(len(axis)):
	indices.append(k) 
	names.append(legend_names[k])

for j in xrange(len(draws)):
	draws_percent[j] = ax.plot(x, z_percent_tot[j], marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_percent_tot.append(draws[j])
	

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=3)

ax.set_title('Translation test No.1'+'\nstep size '+ str(step)+ ' mm', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel('error [%]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs/percent_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs/percent_error.pdf'])

############### figure 3: cumulative error vs frame #############################

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)

draws_abs = [0]*len(axis)

indices = []
names = []

for k in xrange(len(axis)):
	indices.append(k) 
	names.append(legend_names[k])

for j in xrange(len(draws_abs)):
	draws_abs[j] = ax.plot(x, z_abs_errors_tot[j], marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_abs_error_tot.append(draws_abs[j])


box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=3)

ax.set_title('Translation test No.1'+'\nstep size '+ str(step)+ ' mm', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel('cumulative error [mm]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs/cumulative_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs/cumulative_error.pdf'])

plt.show()



