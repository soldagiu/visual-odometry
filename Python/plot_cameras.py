#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt 
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import numpy.matlib
import json 
import math
import itertools
from pylab import *
import pylab as pl
from subprocess import call

import rotazioni

name_txt = ['r3Sift.txt','r4Sift.txt']

path 		= 'Confronto_lori/txt/'

# transformation between camera 1 and camera 2
Eul12 	= [0.02, 0.1, 0.4]			# deg
Eul12 	= map(math.radians, Eul12)	# rad
c1 		= math.cos(Eul12[0])
s1 		= math.sin(Eul12[0])
c2 		= math.cos(Eul12[1])
s2 		= math.sin(Eul12[1])
c3 		= math.cos(Eul12[2])
s3 		= math.sin(Eul12[2])
rot12 	= np.matrix([[c1*c2, c1*s2*s3-c3*s1, s1*s3+c1*c3*s2],
         [c2*s1, c1*c3+s1*s2*s3, c3*s1*s2-c1*s3],
         [-s2, c2*s3, c2*c3]])
trasl12 = np.matrix([546, -1.5, -3.5]).T		# [mm]
Rt12 	= np.concatenate([rot12, trasl12], axis=1)
lastrow	= np.matrix([0,0,0,1])
Rt12 	= np.concatenate([Rt12, lastrow], axis=0)


camera1 = np.eye(4)
camera2 = camera1*Rt12
centro = (camera1+camera2)/2

ang = 0
dx = math.sin(ang)
dz = math.cos(ang)

punti1 = []
punti2 = []
punti_centro = []
dx_tot = []
dz_tot = []

punti1.append(camera1[np.ix_([0,1,2],[3])])
punti2.append(camera2[np.ix_([0,1,2],[3])])
punti_centro.append(centro[np.ix_([0,1,2],[3])])
dx_tot.append(dx)
dz_tot.append(dz)



for i in np.arange(len(name_txt)):

	# read the transformation matrix
	in_file = open(path + name_txt[i], "r")
	text = in_file.read()
	in_file.close()
	dati = json.loads(text.replace('{','[').replace('}',']'))
	for d in xrange(len(dati)): 
		dati[d] = dati[d][0]

	# at each time...
	for d in dati:
		# create the roto-translation 4x4 matrix 
		d.append([0, 0, 0, 1])
		Rt = np.matrix(d)
		R = Rt[0:3,0:3]

		# Eul = np.matlib.zeros((3,1))
		Eul = rotazioni.R_2_Eul(R)

		ang += Eul[1]

		dx = math.sin(ang)
		dz = math.cos(ang)
		# norm = math.hypot(dx,dy)

		dx_tot.append(dx)
		dz_tot.append(dz)

		camera1 = camera1*Rt
		camera2 = camera1*Rt12
		centro = (camera1+camera2)/2
		
		punti1.append(camera1[np.ix_([0,1,2],[3])])
		punti2.append(camera2[np.ix_([0,1,2],[3])])
		punti_centro.append(centro[np.ix_([0,1,2],[3])])


# piano x-z

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)

x = np.arange(len(punti_centro))
ys = [i+x+(i*x)**2 for i in range(len(punti_centro))]
colors = iter(cm.YlOrRd(np.linspace(0.5, 1, len(ys))))

for j in xrange(len(punti_centro)):
	col = next(colors)
	centro = ax.plot(np.array(punti_centro[j][0]), np.array(punti_centro[j][2]), 
		marker='.', markeredgecolor=col, markersize=5, color=col)
	# pl.text(np.array(punti_centro[j][0]-5), np.array(punti_centro[j][2]-5), str(j), color=col, fontsize=12)
	ax = plt.axes()
	x = float(np.array(punti_centro[j][0]))
	z = float(np.array(punti_centro[j][2]))
	# ax.arrow(x, z, dx_tot[j]*300, dz_tot[j]*300, head_width=10, head_length=10, fc=col, ec=col)
	ax.arrow(x, z, dx_tot[j]*30, dz_tot[j]*30, head_width=5, head_length=5, linewidth=3, fc=col, ec=col)
axes().set_aspect('equal','datalim')
ax.set_title('Camera path', fontsize = 14)
plt.xlabel('x [mm]', fontsize = 15)
plt.ylabel('z [mm]', fontsize = 15)
plt.grid()


# plt.ylim(-1000, 11000)

# plt.xlim(-200, 800)
# plt.ylim(-500, 300)

# plt.xlim(0, 500)
# plt.ylim(-200, 100)

plt.xlim(250, 500)
plt.ylim(-350, 50)


plt.savefig('Confronto_lori/camera_paths/tot.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', 'Confronto_lori/camera_paths/tot.pdf'])

# piano x-z - 2 camere

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)

x = np.arange(len(punti1))
ys = [i+x+(i*x)**2 for i in xrange(len(punti1))]
colors1 = iter(cm.Reds(np.linspace(0.5, 1, len(ys))))
colors2 = iter(cm.Blues(np.linspace(0.5, 1, len(ys))))
colors3 = iter(cm.Greys(np.linspace(0.5, 1, len(ys))))
for j in xrange(len(punti1)):
	col1 = next(colors1)
	col2 = next(colors2)
	col3 = next(colors3)	
	# if j==0:
	ax.plot([np.array(punti1[j])[0], np.array(punti2[j])[0]], [np.array(punti1[j])[2],np.array(punti2[j])[2]], 
				color=col3, linestyle='--')
	camera1 = ax.plot(np.array(punti1[j][0]), np.array(punti1[j][2]), 
				marker='.', markeredgecolor=col1, markersize=25, color='red')
	camera2 = ax.plot(np.array(punti2[j][0]), np.array(punti2[j][2]), 
				marker='.', markeredgecolor=col2, markersize=25, color='blue')
	# centro = ax.plot(x0[j], z0[j], marker='^', markeredgecolor=col3, markersize=10, color=col3)
	x1 = float(np.array(punti1[j][0]))
	z1 = float(np.array(punti1[j][2]))
	x2 = float(np.array(punti2[j][0]))
	z2 = float(np.array(punti2[j][2]))
	ax.arrow(x1, z1, dx_tot[j]*150, dz_tot[j]*150, linewidth=3, head_width=40, head_length=30, fc='red', color='red')
	ax.arrow(x2, z2, dx_tot[j]*150, dz_tot[j]*150, linewidth=3, head_width=40, head_length=30, fc='blue', color='blue')
	# else:
	# 	camera1 = ax.plot(np.array(punti1[j][0]), np.array(punti1[j][2]), 
	# 				marker='.', markeredgecolor='w', markersize=10, color='w')
	# 	camera2 = ax.plot(np.array(punti2[j][0]), np.array(punti2[j][2]), 
	# 				marker='.', markeredgecolor='w', markersize=10, color='w')

# plt.xlim(-100, 650)
# plt.ylim(-200, 1600)		# traslazione 1
# plt.ylim(-500, 350)		# rotazione 1
# plt.ylim(-1000, 11000)
plt.ylim(-200, 1700)

axes().set_aspect('equal', 'datalim')


# ax.set_title('Camera path - Translation sequence No.1', fontsize = 14)
plt.xlabel('x [mm]', fontsize = 15)
plt.ylabel('z [mm]', fontsize = 15)
plt.grid()





plt.savefig('Confronto_lori/camera_paths/xz.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', 'Confronto_lori/camera_paths/xz.pdf'])








plt.draw()
plt.show()



