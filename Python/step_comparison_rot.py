import matplotlib.pyplot as plt 
import numpy as np
import numpy.matlib
import json 
import math
from subprocess import call

# steps_deg 		= [1, 3, 5, 10, 15]	
steps_deg 		= [3, 5, 10, 15, 20]	
cases 			= [1,1,1,1,1]

dataset 		= "1"

name_txt = (str(steps_deg[0])+'.txt', str(steps_deg[1])+'.txt', 
			str(steps_deg[2])+'.txt', str(steps_deg[3])+'.txt', 
			str(steps_deg[4])+'.txt')

axis 			= 1;	# 0-x, 1-y, 2-z
if axis==0:
	axis_name 	= "x"
elif axis==1:
	axis_name 	= "y"
else:
	axis_name	= "z"

n = 1;		
# 0 for KNEIP
# 1 for GAO
# 2 for EPNP
if n == 0:
    string_name = "Kneip"
    string_graph = "KNEIP"
    colors 	= [[127,199,175],[255,61,127],[255,158,157],  
    			[100,54,255],[0,160,176]]
elif n == 1:
    string_name = "Gao"
    string_graph = "GAO"
    colors 	= [[180,222,193], [92,88,99], [255,31,76],
 		 		[168,81,99], [63,184,175]]
elif n == 2:
    string_name = "Epnp"
    string_graph = "EPNP"
    colors 	= [[249,183,122], [255,128,108],[98,127,105], 
    			[208,75,102], [125,65,91]]


steps 			=  map(lambda x:math.radians(np.array(x)), steps_deg)				

path 			= "../RESULTS/STEPS_"+dataset+"/"+string_name+'_rot/'
path_length 	= 90				# [deg]

camera1 		= np.matrix([0, 0, 0]).T
camera1_set 	= np.matrix([0, 0, 0]).T
expected_point 	= np.matrix([0, 0, 0]).T
error 			= np.matrix([0, 0, 0]).T
tot_abs_error 	= np.matrix([0, 0, 0]).T

colors 			= map(lambda x:np.array(x)/255., colors)		
markers 		= ('s', 'o', 'v', 'D', 'H', '>')
mar_size		= 5
legend_names 	= (str(steps_deg[0])+' deg', str(steps_deg[1])+' deg', 
					str(steps_deg[2])+' deg', str(steps_deg[3])+' deg', 
					str(steps_deg[4])+' deg')

errors 				= []
y_errors_tot 		= []
draws_error_tot 	= []
abs_errors 			= []
y_abs_errors_tot 	= []
draws_abs_error_tot = []
percent_errors		= []
y_percent_errors 	= []
draws_percent		= []
y_percent_tot 		= []
draws_percent_tot 	= []

for i in xrange(len(name_txt)):

	if cases[i]:

		# read the transformation matrix
		in_file = open(path + name_txt[i],"r")
		text = in_file.read()
		in_file.close()
		dati = json.loads(text.replace('{','[').replace('}',']'))
		for d in xrange(len(dati)): 
			dati[d] = dati[d][0]

		# at each time...
		for d in dati:

			# expected point after the y axis rotation
			new_expected_point = expected_point + np.matrix([0, steps[i], 0]).T

			# create the roto-translation 4x4 matrix 
			Rt = np.matrix(d)
			R = Rt[0:3,0:3]

			Eul = np.matlib.zeros((3,1))
			 
			Eul[0] = math.atan2(R[1,0],R[0,0]);
			Eul[1] = (math.atan2(-R[2,0], math.sqrt(math.pow(R[0,0],2) + 
					math.pow(R[1,0],2)))+ math.atan2(-R[2,0], 
					math.sqrt(math.pow(R[2,1],2)+math.pow(R[2,2],2))))/2;
			Eul[2] = math.atan2(R[2,1],R[2,2]);
			
			# apply the transformation to the left camera center
			new_camera1 = camera1 + Eul
			new_camera1_set = camera1_set + Eul

			# calculate the rotation error
			new_error = new_camera1[0:3] - new_expected_point
			errors.append(new_error)

			# % error
			percent = abs(new_error[axis])/new_expected_point[1]*100;
			percent_errors.append(percent)

			# calculate the cumulative rotation error
			expected_step = np.matrix([0, steps[i], 0]).T
			new_abs_error = new_camera1_set[0:3] - expected_step
			new_tot_abs_error = abs(tot_abs_error) + abs(new_abs_error)
			# print new_tot_abs_error[1]
			abs_errors.append(new_tot_abs_error)
			
			# redefine the variables
			error = new_error
			camera1 = new_camera1
			expected_point = new_expected_point
			tot_abs_error = new_tot_abs_error


		# define a vector that contains the errors at each step
		y_errors = [0]
		for e in errors:
			y_errors.append(math.degrees(float(e[axis])))
		errors = []
		y_errors_tot.append(y_errors)

		y_percent_errors = [0]
		for e in percent_errors:
			y_percent_errors.append(float(e))
		percent_errors = []
		y_percent_tot.append(y_percent_errors)

		y_abs_errors = [0]
		for e in abs_errors:
			y_abs_errors.append(math.degrees(float(e[axis])))
		abs_errors = []
		y_abs_errors_tot.append(y_abs_errors)

	# repeat the cicle for each algorithm considered 
	camera1 = np.matrix([0, 0, 0]).T
	expected_point = np.matrix([0, 0, 0]).T
	error = np.matrix([0, 0, 0]).T
	tot_abs_error = np.matrix([0, 0, 0]).T



############# figure 1: real error vs real z axis translation #################

fig = plt.figure()
ax = fig.add_subplot(111)

draws = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws)):
	frame = [steps_deg[j]*2*i for i in xrange(path_length/steps_deg[j]+1)]
	draws[j] = ax.plot(frame, y_errors_tot[j], marker = markers[indices[j]],
			 	markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 	color = colors[indices[j]])
	draws_error_tot.append(draws[j])
	
# Shrink current axis by 20%
# box = ax.get_position()
# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
# # Put a legend to the right of the current axis
# ax.legend(map(lambda x:x[0],draws_error_tot), names, numpoints = 1,
# 		loc='center left', bbox_to_anchor=(1, 0.5),
#            ncol=1, fancybox=True, shadow=True, prop={'size':10})

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=3)

ax.set_title('Rotation test No.'+dataset+'\n'+string_graph+ ' algorithm with different step size', fontsize=14)
plt.xlabel('y-axis rotation [deg]', fontsize = 15)
plt.ylabel(axis_name+'-axis error [deg]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_'+axis_name+'/error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/error.pdf'])

# figure 2: % error vs real z axis translation

fig = plt.figure()
ax = fig.add_subplot(111)

draws_percent = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws)):
	frame = [steps_deg[j]*i for i in xrange(path_length/steps_deg[j]+1)]
	draws[j] = ax.plot(frame, y_percent_tot[j], marker = markers[indices[j]],
			 	markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 	color = colors[indices[j]])
	draws_error_tot.append(draws[j])
	
# # Shrink current axis by 20%
# box = ax.get_position()
# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
# # Put a legend to the right of the current axis
# ax.legend(map(lambda x:x[0],draws_error_tot), names, numpoints = 1,
# 		loc='center left', bbox_to_anchor=(1, 0.5),
#            ncol=1, fancybox=True, shadow=True, prop={'size':10})

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=3)

ax.set_title('Rotation test No.'+dataset+'\n'+string_graph+ ' algorithm with different step size', fontsize=14)
plt.xlabel('y-axis rotation [deg]', fontsize = 15)
plt.ylabel(axis_name+'-axis rotation error [%]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_'+axis_name+'/percent_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/percent_error.pdf'])

################### figure 3: cumulative error vs frame #######################

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)


draws_abs = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws_abs)):
	frame = [steps_deg[j]*i for i in xrange(path_length/steps_deg[j]+1)]
	draws_abs[j] = ax.plot(frame, y_abs_errors_tot[j], marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_abs_error_tot.append(draws_abs[j])
	
# # Shrink current axis by 20%
# box = ax.get_position()
# ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
# # Put a legend to the right of the current axis
# ax.legend(map(lambda x:x[0],draws_error_tot), names, numpoints = 1,
# 		loc='center left', bbox_to_anchor=(1, 0.5),
#            ncol=1, fancybox=True, shadow=True, prop={'size':10})

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=3)

ax.set_title('Rotation test No.'+dataset+'\n'+string_graph+ ' algorithm with different step size', fontsize=14)
plt.xlabel('y-axis rotation [deg]', fontsize = 15)
plt.ylabel(axis_name+'-axis cumulative error [deg]', fontsize = 15)
plt.grid()
plt.savefig(path+'Graphs_'+axis_name+'/cumulative_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/cumulative_error.pdf'])

plt.show()

