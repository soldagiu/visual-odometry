#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt 
import matplotlib.cm as cm
from mpl_toolkits.mplot3d import Axes3D
import mpl_toolkits.mplot3d as mp3d
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d
from mpl_toolkits.mplot3d.art3d import Poly3DCollection
import numpy as np
import numpy.matlib
import json 
import math
import itertools

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)


# 3D points coordinates
X3D = [-2269.3, -1916.09, 3392.88, 3286.85]
Y3D = [-1923.78, 132.92, -1098.84, -1066.07]
Z3D = [13781.4, 4463.83, 4803.58, 7854.43 ] 

# bearing vectors coordinates
x2D = [-0.161881, -0.398205, 0.570612, 0.384839]
y2D = [-0.137402, 0.0273526, -0.183947, -0.124833]
z2D = [0.977198, 0.916888, 0.800353, 0.914503] 
scala = 4000
x2D = map(lambda x:np.array(x)*scala, x2D)
y2D = map(lambda x:np.array(x)*scala, y2D)
z2D = map(lambda x:np.array(x)*scala, z2D)

# 2D points coordinates
x_trasl = 1020;
y_trasl = 543;
z_2d = 1133;
x_img = [1866.1687, 566.103088, 1535.12439, 870.529175]
y_img = [264.264404, 558.514343, 370.023712, 365.371948]
z_img = [z_2d, z_2d, z_2d, z_2d] 
x_img = map(lambda x:np.array(x)-x_trasl, x_img)
y_img = map(lambda x:np.array(x)-y_trasl, y_img)

fig = plt.figure()

# plot 3D points
col = [1,0,0]
ax = fig.add_subplot(111, projection='3d')
ax.scatter(X3D,Y3D,Z3D, s=50, color=col, marker='*')

# plot bearing vectors
# ax = fig.add_subplot(111, projection='3d')
# ax.plot(x2D, y2D, z2D, 'o', markersize=1, color='green', alpha=0.2)
# ax.plot([0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], 'o', markersize=1, color='green', alpha=0.5)
for v in xrange(len(x2D)):
    ax.plot([0, x2D[v]], [0, y2D[v]], [0, z2D[v]], color='red', alpha=0.8, lw=1)
    a = Arrow3D([0, x2D[v]], [0, y2D[v]], [0, z2D[v]], mutation_scale=10, lw=1, arrowstyle="-|>", color="r")
    ax.add_artist(a)

# plot left camera image plane
x_dim = 1020;
y_dim = 543;
z_dim = z_2d;
# vertices = [(-x_dim, y_dim, z_dim), 
# 			(x_dim, y_dim, z_dim), 
# 			(x_dim, -y_dim, z_dim), 
# 			(-x_dim, -y_dim, z_dim)]
x = [-x_dim, x_dim, x_dim, -x_dim]
y = [y_dim, y_dim, -y_dim, -y_dim]
z = [z_dim,z_dim,z_dim,z_dim]
verts = [zip(x, y,z)]
plane = Poly3DCollection(verts, facecolors='b', linewidths=0.5)

alpha = 0.2
plane.set_facecolor((0, 0, 1, alpha))
ax.add_collection3d(plane)

# plot 2D points
col2 = [0,0,1]
# ax = fig.add_subplot(111, projection='3d')
ax.scatter(x_img,y_img,z_img, s=20, color=col2, marker='o')

ax.set_title('2D-3D correspondences')
# ax.set_xlabel('x [mm]')
# ax.set_ylabel('y [mm]')
# ax.set_zlabel('z [mm]')
# ax.xaxis._axinfo['label']['space_factor'] = 2
# ax.yaxis._axinfo['label']['space_factor'] = 2
# ax.zaxis._axinfo['label']['space_factor'] = 2
frame1 = plt.gca()
for xlabel_i in frame1.axes.get_xticklabels():
    xlabel_i.set_visible(False)
    xlabel_i.set_fontsize(0.0)
for xlabel_i in frame1.axes.get_yticklabels():
    xlabel_i.set_fontsize(0.0)
    xlabel_i.set_visible(False)
for xlabel_i in frame1.axes.get_zticklabels():
    xlabel_i.set_fontsize(0.0)
    xlabel_i.set_visible(False)
for tick in frame1.axes.get_xticklines():
    tick.set_visible(False)
for tick in frame1.axes.get_yticklines():
    tick.set_visible(False)
for tick in frame1.axes.get_zticklines():
    tick.set_visible(False)

plt.axis('equal')

plt.savefig('Graphs/2D_3D_correspondences.eps', format='eps', dpi=1000)

plt.show()