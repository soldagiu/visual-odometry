
from PIL import Image
from os import listdir
from images2gif import writeGif
from time import time

#Get the file names
mypath = '/Users/giuliasolda/Desktop/immagini_gif/Left_t1/'
file_names = [f for f in listdir(mypath)]
print file_names

images = [Image.open(mypath + f) for f in file_names]

#Set to 24FPS
runningtime = 0.0416
# print runningtime

print 'Saving'
gif_path = "/Users/giuliasolda/Desktop/immagini_gif/Gif/" 
writeGif(gif_path +str(int(time())) + ".gif", images, duration=0.3, dither=1)

