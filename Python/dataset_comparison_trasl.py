import matplotlib.pyplot as plt 
import numpy as np
import json 
import math
from subprocess import call

steps 		= [50, 200, 50, 200]				# [mm]
datasets 	= [1,1,2,2]
name_txt 	= (str(steps[0])+'_1.txt', str(steps[1])+'_1.txt', 
				str(steps[2])+'_2.txt', str(steps[3])+'_2.txt', )

cases 		= [1,1,1,1]

axis 			= 2;	# 0-x, 1-y, 2-z
if axis==0:
	axis_name 	= "x"
elif axis==1:
	axis_name 	= "y"
else:
	axis_name	= "z"

n = 1;		
# 0 for KNEIP
# 1 for GAO
# 2 for EPNP
if n == 0:
    string_name = "Kneip"
    string_graph = "KNEIP"
    colors 	= [[100,54,255],[255,61,127],
   				[255,158,157],[0,160,176]]
elif n == 1:
    string_name = "Gao"
    string_graph = "GAO"
    colors 	= [[180,222,193], [92,88,99], [255,31,76],
 		 		[168,81,99], [63,184,175]]
elif n == 2:
    string_name = "Epnp"
    string_graph = "EPNP"
    colors 	= [[249,183,122], [255,128,108],[98,127,105], 
    			[208,75,102], [125,65,91]]


path 			= "../RESULTS/DATASETS/"+string_name+'_trasl/'



camera1 		= np.matrix([0, 0, 0, 1]).T
camera1_set 	= np.matrix([0, 0, 0, 1]).T
expected_point 	= np.matrix([0, 0, 0, 1]).T
error 			= np.matrix([0, 0, 0]).T
tot_abs_error 	= np.matrix([0, 0, 0]).T


colors 			= map(lambda x:np.array(x)/255., colors)
markers 		= ('o', '>', 's', 'v', 'D', 'H')
mar_size		= 5
legend_names 	= ('dataset '+ str(datasets[0])+ ' - ' + str(steps[0])+' mm', 
					'dataset '+str(datasets[1])+ ' - ' + str(steps[1])+' mm',
					'dataset '+str(datasets[2])+ ' - ' + str(steps[2])+' mm', 
					'dataset '+str(datasets[3])+ ' - ' + str(steps[3])+' mm')

errors 				= []
z_errors_tot 		= []
draws_error_tot 	= []
abs_errors 			= []
z_abs_errors_tot 	= []
draws_abs_error_tot = []
percent_errors		= []
z_percent_errors 	= []
draws_percent		= []
z_percent_tot 		= []
draws_percent_tot 	= []

for i in xrange(len(name_txt)):

	if cases[i]:

		# read the transformation matrix
		in_file = open(path + name_txt[i],"r")
		text = in_file.read()
		in_file.close()
		dati = json.loads(text.replace('{','[').replace('}',']'))
		for d in xrange(len(dati)): 
			dati[d] = dati[d][0]

		# at each time...
		for d in dati:

			# expected point after the z axis translation
			new_expected_point = expected_point[0:3] + np.matrix([0, 0, steps[i]]).T

			# create the roto-translation 4x4 matrix 
			d.append([0, 0, 0, 1])
			Rt = np.matrix(d)

			# apply the transformation to the left camera center
			new_camera1 = Rt*camera1
			new_camera1_set = Rt*camera1_set

			# calculate the translation error
			new_error = new_camera1[0:3] - new_expected_point
			errors.append(new_error)

			# % error
			percent = abs(new_error[axis])/new_expected_point[2]*100;
			percent_errors.append(percent)

			# calculate the cumulative translation error
			expected_step = np.matrix([0, 0, steps[i]]).T
			new_abs_error = new_camera1_set[0:3] - expected_step
			new_tot_abs_error = abs(tot_abs_error) + abs(new_abs_error)
			abs_errors.append(new_tot_abs_error)

			# redefine the variables
			error = new_error
			camera1 = new_camera1
			expected_point = new_expected_point
			tot_abs_error = new_tot_abs_error

		# define a vector that contains the errors at each step
		z_errors = [0]
		for e in errors:
			z_errors.append(float(e[axis]))
		errors = []
		z_errors_tot.append(z_errors)

		z_percent_errors = [0]
		for e in percent_errors:
			z_percent_errors.append(float(e))
		percent_errors = []
		z_percent_tot.append(z_percent_errors)

		z_abs_errors = [0]
		for e in abs_errors:
			z_abs_errors.append(float(e[axis]))
		abs_errors = []
		z_abs_errors_tot.append(z_abs_errors)

	# repeat the cicle for each algorithm considered 
	camera1 = np.matrix([0, 0, 0, 1]).T
	expected_point = np.matrix([0, 0, 0, 1]).T
	error = np.matrix([0, 0, 0]).T
	tot_abs_error = np.matrix([0, 0, 0]).T


############## figure 1: real error vs real z axis translation ################

fig = plt.figure()
ax = fig.add_subplot(111)


draws = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws)):
	if datasets[j]==1:
		path_length 	= 1350			
	else:
		path_length 	= 1340
	frame = [steps[j]*i for i in xrange(path_length/steps[j]+1)]
	draws[j] = ax.plot(frame, z_errors_tot[j], marker = markers[indices[j]],
			 	markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 	color = colors[indices[j]])
	draws_error_tot.append(draws[j])
	


box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=2)

ax.set_title(string_graph+ ' algorithm with different datasets', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel(axis_name+'-axis translation error [mm]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_'+axis_name+'/error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/error.pdf'])

################### figure 2: % error vs real z axis translation #####################

fig = plt.figure()
ax = fig.add_subplot(111)

draws_percent = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws)):
	if datasets[j]==1:
		path_length 	= 1350			
	else:
		path_length 	= 1340
	frame = [steps[j]*i for i in xrange(path_length/steps[j]+1)]
	draws_percent[j] = ax.plot(frame, z_percent_tot[j], marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_percent_tot.append(draws[j])
	
	

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=2)

ax.set_title(string_graph+ ' algorithm with different datasets', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel(axis_name+'-axis translation error [%]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_'+axis_name+'/percent_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/percent_error.pdf'])

####################### figure 3: cumulative error vs frame ########################

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)


draws_abs = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws_abs)):
	if datasets[j]==1:
		path_length 	= 1350			
	else:
		path_length 	= 1340
	frame = [steps[j]*i for i in xrange(path_length/steps[j]+1)]
	draws_abs[j] = ax.plot(frame, z_abs_errors_tot[j], marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_abs_error_tot.append(draws_abs[j])
	


box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_error_tot), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.15),
        fancybox=True, shadow=False, ncol=2)

ax.set_title(string_graph+ ' algorithm with different datasets', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel(axis_name+'-axis cumulative error [mm]', fontsize = 15)
plt.grid()

plt.savefig(path+'Graphs_'+axis_name+'/cumulative_error.pdf', format='pdf', dpi=1000)
call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/cumulative_error.pdf'])


plt.show()
