#!/usr/bin/env python
# -*- coding: utf-8 -*-

import matplotlib.pyplot as plt 
import numpy as np
import json 
import math

from subprocess import call

name_txt = ('RtRansacKneip.txt', 'RtRansacGao.txt', 'RtRansacEpnp.txt',
			'RtNonLinKneip.txt', 'RtNonLinGao.txt', 'RtNonLinEpnp.txt')

# Cases to be considered:
# [0]   RANSAC with KNEIP algorithm
# [1]   RANSAC with GAO algorithm
# [2]   RANSAC with EPNP algorithm
# [3]   NON-LINEAR OPTIMIZATION after RANSAC - KNEIP
# [4]   NON-LINEAR OPTIMIZATION after RANSAC - GAO
# [5]   NON-LINEAR OPTIMIZATION after RANSAC - EPNP
cases 			= [1,1,1,1,1,1]

dataset 		= "2"

axis 			= 2;	# 0-x, 1-y, 2-z
if axis==0:
	axis_name 	= "x"
elif axis==1:
	axis_name 	= "y"
else:
	axis_name	= "z"

step 			= 300				# [mm]
path 			= "../RESULTS/METHODS/Translation"+dataset+"_"+str(step)+"mm/"
if dataset=="1":
	path_length 	= 1350				# [mm]
else:
	path_length 	= 1340


camera1 		= np.matrix([0, 0, 0, 1]).T
camera1_set 	= np.matrix([0, 0, 0, 1]).T
expected_point 	= np.matrix([0, 0, 0, 1]).T
expected_step 	= np.matrix([0, 0, step]).T
error 			= np.matrix([0, 0, 0]).T
tot_abs_error 	= np.matrix([0, 0, 0]).T

colors 			= [[85,98,112], [78,205,196], [199,244,100],
		 			[204,153,255], [255,107,107], [196,77,88]]
colors 			= map(lambda x:np.array(x)/255., colors)
markers 		= ('s', 'o', 'v', 'D', 'H', '>')
mar_size		= 5
legend_names 	= ('Ransac - Kneip', 'Ransac - Gao', 'Ransac - Epnp',
					'Non-lin. optim. (Kneip)', 'Non-lin. optim. (Gao)',
					'Non-lin. optim. (Epnp)')

errors 				= []
z_errors_tot 		= []
draws_error_tot 	= []
abs_errors 			= []
z_abs_errors_tot 	= []
draws_abs_error_tot = []
percent_errors		= []
z_percent_errors 	= []
draws_percent		= []
z_percent_tot 		= []
draws_percent_tot 	= []

for i in xrange(len(name_txt)):

	if cases[i]:

		# read the transformation matrix
		in_file = open(path + name_txt[i],"r")
		text = in_file.read()
		in_file.close()
		dati = json.loads(text.replace('{','[').replace('}',']'))
		for d in xrange(len(dati)): 
			dati[d] = dati[d][0]

		# at each time...
		for d in dati:

			# expected point after the z axis translation
			new_expected_point = expected_point[0:3] + np.matrix([0, 0, step]).T

			# create the roto-translation 4x4 matrix 
			d.append([0, 0, 0, 1])
			Rt = np.matrix(d)

			# apply the transformation to the left camera center
			new_camera1 = Rt*camera1
			new_camera1_set = Rt*camera1_set

			# calculate the translation error
			new_error = new_camera1[0:3] - new_expected_point
			errors.append(new_error)

			# % error
			percent = abs(new_error[axis])/new_expected_point[2]*100;
			percent_errors.append(percent)

			# calculate the cumulative translation error
			new_abs_error = new_camera1_set[0:3] - expected_step
			new_tot_abs_error = abs(tot_abs_error) + abs(new_abs_error)
			abs_errors.append(new_tot_abs_error)

			# redefine the variables
			error = new_error
			camera1 = new_camera1
			expected_point = new_expected_point
			tot_abs_error = new_tot_abs_error

		# define a vector that contains the errors at each step
		z_errors = [0]
		for e in errors:
			z_errors.append(float(e[axis]))
		errors = []
		z_errors_tot.append(z_errors)

		z_percent_errors = [0]
		for e in percent_errors:
			z_percent_errors.append(float(e))
		percent_errors = []
		z_percent_tot.append(z_percent_errors)

		z_abs_errors = [0]
		for e in abs_errors:
			z_abs_errors.append(float(e[axis]))
		abs_errors = []
		z_abs_errors_tot.append(z_abs_errors)



	# repeat the cicle for each algorithm considered 
	camera1 = np.matrix([0, 0, 0, 1]).T
	expected_point = np.matrix([0, 0, 0, 1]).T
	error = np.matrix([0, 0, 0]).T
	tot_abs_error = np.matrix([0, 0, 0]).T


# 3 Kneip, 4 Gao, 5 Epnp
case = 5		
print z_abs_errors_tot[case]
# print z_abs_errors_tot[case][-1]
# print len(z_abs_errors_tot[case][1:])
mean_error = (z_abs_errors_tot[case][-1])/len(z_abs_errors_tot[case][1:])
print mean_error

# ############# figure 1: real error vs real z axis translation #####################

# fig = plt.figure()
# ax = fig.add_subplot(111)
# x = np.array(xrange(int(path_length/step)+1))*step

# draws = [0]*sum(cases)

# indices = []
# names = []

# for k in xrange(len(cases)):
# 	if cases[k]:
# 		indices.append(k) 
# 		names.append(legend_names[k])


# for j in xrange(len(draws)):

# 	draws[j] = ax.plot(x, z_errors_tot[j], marker = markers[indices[j]],
# 			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
# 			 color = colors[indices[j]])
# 	draws_error_tot.append(draws[j])
	
# # # Shrink current axis by 20%
# # box = ax.get_position()
# # ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
# # # Put a legend to the right of the current axis
# # ax.legend(map(lambda x:x[0],draws_error_tot), names, numpoints = 1,
# # 		loc='center left', bbox_to_anchor=(1, 0.5),
# #            ncol=1, fancybox=True, shadow=True, prop={'size':10})

# box = ax.get_position()
# ax.set_position([box.x0, box.y0 + box.height * 0.2,
#                  box.width*0.7, box.height * 0.8])

# ax.legend(map(lambda x:x[0],draws_error_tot), names,
# 		loc='upper center', bbox_to_anchor=(0.5, -0.12),
#         fancybox=True, shadow=False, ncol=2)

# ax.set_title('Translation test No.'+dataset+'\nstep size '+ str(step)+ ' mm', fontsize = 14)
# plt.xlabel('z-axis translation [mm]', fontsize = 15)
# plt.ylabel(axis_name+'-axis translation error [mm]', fontsize = 15)
# plt.grid()

# plt.savefig(path+'Graphs_'+axis_name+'/error.pdf', format='pdf', dpi=1000)
# call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/error.pdf'])

# ##################### figure 2: % error vs real z axis translation ###########################

# fig = plt.figure()
# ax = fig.add_subplot(111)
# x = np.array(xrange(int(path_length/step)+1))*step

# draws_percent = [0]*sum(cases)

# indices = []
# names = []

# for k in xrange(len(cases)):
# 	if cases[k]:
# 		indices.append(k) 
# 		names.append(legend_names[k])

# for j in xrange(len(draws)):
# 	draws_percent[j] = ax.plot(x, z_percent_tot[j], marker = markers[indices[j]],
# 			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
# 			 color = colors[indices[j]])
# 	draws_percent_tot.append(draws[j])
	
# # # Shrink current axis by 20%
# # box = ax.get_position()
# # ax.set_position([box.x0, box.y0, box.width * 0.7, box.height])
# # # Put a legend to the right of the current axis
# # ax.legend(map(lambda x:x[0],draws_error_tot), names, numpoints = 1,
# # 		loc='center left', bbox_to_anchor=(1, 0.5),
# #            ncol=1, fancybox=True, shadow=True, prop={'size':10})

# box = ax.get_position()
# ax.set_position([box.x0, box.y0 + box.height * 0.2,
#                  box.width*0.7, box.height * 0.8])

# ax.legend(map(lambda x:x[0],draws_error_tot), names,
# 		loc='upper center', bbox_to_anchor=(0.5, -0.12),
#         fancybox=True, shadow=False, ncol=2)

# ax.set_title('Translation test No.'+dataset+'\nstep size '+ str(step)+ ' mm', fontsize = 14)
# plt.xlabel('z-axis translation [mm]', fontsize = 15)
# plt.ylabel(axis_name+'-axis translation error [%]', fontsize = 15)
# plt.grid()

# plt.savefig(path+'Graphs_'+axis_name+'/percent_error.pdf', format='pdf', dpi=1000)
# call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/percent_error.pdf'])

############### figure 3: cumulative error vs frame #############################

fig_abs = plt.figure()
ax = fig_abs.add_subplot(111)
x = np.array(xrange(int(path_length/step)+1))*step

draws_abs = [0]*sum(cases)

indices = []
names = []

for k in xrange(len(cases)):
	if cases[k]:
		indices.append(k) 
		names.append(legend_names[k])

for j in xrange(len(draws_abs)):
	draws_abs[j] = ax.plot(x, z_abs_errors_tot[j], marker = markers[indices[j]],
			 markersize = mar_size, markeredgecolor = colors[indices[j]], 
			 color = colors[indices[j]])
	draws_abs_error_tot.append(draws_abs[j])

box = ax.get_position()
ax.set_position([box.x0, box.y0 + box.height * 0.2,
                 box.width*0.7, box.height * 0.8])

ax.legend(map(lambda x:x[0],draws_abs), names,
		loc='upper center', bbox_to_anchor=(0.5, -0.12),
        fancybox=True, shadow=False, ncol=2)

ax.set_title('Translation test No.'+dataset+'\nstep size '+ str(step)+ ' mm', fontsize = 14)
plt.xlabel('z-axis translation [mm]', fontsize = 15)
plt.ylabel(axis_name+'-axis cumulative error [mm]', fontsize = 15)
plt.grid()

# plt.savefig(path+'Graphs_'+axis_name+'/cumulative_error.pdf', format='pdf', dpi=1000)
# call(['pdfcrop', '--margins', '10', path+'Graphs_'+axis_name+'/cumulative_error.pdf'])

plt.show()



