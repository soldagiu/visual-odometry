/*! \file visual_odometry.h
 *
 */


#ifndef VISUAL_ODOMETRY_H
#define VISUAL_ODOMETRY_H

#include <iostream>
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#ifdef VO_WITH_NONFREE
#include <opencv2/nonfree/nonfree.hpp>
#endif

#ifdef VO_WITH_CUDA
#include "opencv2/cudafeatures2d.hpp"
#include "opencv2/xfeatures2d/cuda.hpp"
#endif

#include <opengv/absolute_pose/methods.hpp>
#include <opengv/absolute_pose/CentralAbsoluteAdapter.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/triangulation/methods.hpp>
#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/absolute_pose/AbsolutePoseSacProblem.hpp>

#include <chrono>
#include <unistd.h>

/*! @class visualOdometry
 * @brief Visual odometry class.
 *
 * This class implements a Visual Odometry algorithm using a 2D-to-3D method
 * inside a RANSAC outlier rejection scheme.
 * The motion estimation algorithm is selectable from: KNEIP, GAO, EPNP.
 * A non-linear optimization can also be applied.
 * The feature detector is selectable from STAR, SIFT, SURF, FAST and SURF on GPU,
 * while the feature descriptor is selectable from BRIEF, SIFT, SURF and SURF on GPU.
 */
class visualOdometry
{
public:

    /*! @brief The class constructor that sets detector and descriptor (see Feature enum).
     *
     */
    visualOdometry(int feat = 0)
    {
        feature = feat;

        if (feature==SIFT_FEATURE)
        {
            #ifdef VO_WITH_NONFREE
            featuresSift = cv::SIFT(0, 3, 0.08);
            #else
            bool SIFT_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE = false;
            assert(SIFT_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE);
            #endif
        }
        if (feature==SURF_FEATURE)
        {
            #ifdef VO_WITH_NONFREE
            detectorSurf = cv::SurfFeatureDetector(1000, 4, 2, true, false);
            #else
            bool SURF_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE = false;
            assert(SURF_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE);
            #endif
        }
        if (feature==FAST_BRIEF_FEATURE)
        {
            #ifdef VO_WITH_NONFREE
            #else
            bool FAST_BRIEF_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE = false;
            assert(FAST_BRIEF_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE);
            #endif
        }
//        if (feature==ORB_FEATURE)
//        {
//            #ifdef VO_WITH_NONFREE
//            featureOrb = cv::ORB(1000);
////            matcher.create(matcher.BRUTEFORCE_HAMMING);
////            matcher = cv::BFMatcher.create(cv::BFMatcher.BRUTEFORCE_HAMMING);
//            orbMatcher = cv::DescriptorMatcher::create("BruteForce-Hamming");
//            #else
//            bool ORB_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE = false;
//            assert(ORB_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE);
//            #endif
//        }
//        if (feature==BRISK_FEATURE)
//        {
//            #ifdef VO_WITH_NONFREE
//            featureBrisk = cv::BRISK(5,5);
//            #else
//            bool BRISK_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE = false;
//            assert(BRISK_NOT_AVAILABLE_COMPILE_USING_VO_WITH_NONFREE);
//            #endif
//        }
        if (feature==SURF_GPU_FEATURE)
        {
            #ifdef VO_WITH_CUDA
            featuresSurfGPU = cv::cuda::SURF_CUDA(500);
            cudaMatcher = cv::cuda::DescriptorMatcher::createBFMatcher(featuresSurfGPU.defaultNorm());
            #else
            bool SURF_CUDA_NOT_AVAILABLE_COMPILE_USING_VO_WITH_CUDA = false;
            assert(SURF_CUDA_NOT_AVAILABLE_COMPILE_USING_VO_WITH_CUDA);
            #endif
        }
    };
    
//    void drawBestMatches(std::vector <cv::DMatch> & matches, cv::Mat & Img1, cv::Mat & Img2, std::vector<cv::KeyPoint> & keypointsL, std::vector<cv::KeyPoint> & keypointsR, int numPoints, int counter, double resize, std::string path)
//    {
//        std::stringstream ss;
//        ss.str("");
//        ss << counter;
//        // keypoints on the camera planes
//        cv::Mat points2DL;
//        cv::Mat points2DR;
//        // sort keypoints on both the camera planes (with respect to matches)
//        points2DL = cv::Mat(2, matches.size(), CV_64F);
//        points2DR = cv::Mat(2, matches.size(), CV_64F);
//        for (int i=0; i<matches.size(); i++)
//        {
//            points2DL.at<float>(0,i) = keypointsL[matches[i].queryIdx].pt.x;
//            points2DL.at<float>(1,i) = keypointsL[matches[i].queryIdx].pt.y;
//            points2DR.at<float>(0,i) = keypointsR[matches[i].trainIdx].pt.x;
//            points2DR.at<float>(1,i) = keypointsR[matches[i].trainIdx].pt.y;
//        }
//        // Draw circle around the best features
//        // create a 1x1 matrix
//        cv::Mat colour(1,1, CV_8UC3, cvScalar(0,0,0));
//        for (int ind=0; ind<numPoints; ind++)
//        {
//            // convert the matrix to hsv
//            cvtColor(colour, colour, CV_BGR2HSV);
//            // initialize an hsv colour
//            colour.at<cv::Vec3b>(0,0)=cv::Vec3b(ind*255/numPoints,255,255);
//            // convert to rgb
//            cvtColor(colour, colour, CV_HSV2BGR);
//            // the rgb colour is then...
//            cv::Vec3b colourInRGB = colour.at<cv::Vec3b>(0,0);
//            uchar blue = colourInRGB[0];
//            uchar green = colourInRGB[1];
//            uchar red = colourInRGB[2];
//            cv::circle(Img1, cv::Point2i(points2DL.at<float>(0,ind),points2DL.at<float>(1,ind)), keypointsL[matches[ind].queryIdx].size, cv::Scalar_<float>(blue, green, red), 3);
//            cv::circle(Img2, cv::Point2i(points2DR.at<float>(0,ind),points2DR.at<float>(1,ind)), keypointsR[matches[ind].trainIdx].size, cv::Scalar_<float>(blue, green, red), 3);
//        };
//        // Draw best matches
//        cv::Mat Im_check;
//        hconcat(Img1, Img2, Im_check);
//        //showFigure(Im_check, 1280, 341);
//        cv::resize(Im_check, Im_check, cv::Size(Im_check.cols/resize,Im_check.rows/resize));
//        cv::imwrite(path + ss.str() + ".jpg", Im_check);
//    }

    
    /*! @brief Adds the left image (and eventually correct it).
     *
     * calls addLeftImg(cv::Mat &ImgIn, bool correct)
     * 
     * @param ImgIn The input image.
     * @param correct True to undistort the image.
     * @return The left image.
     */
    void addLeftImg(cv::Mat &ImgIn, bool correct)
    {
        if (correct){
            // undistort
            cv::undistort(ImgIn, Img1, CM1, K1);
        }
        else
            Img1 = ImgIn;
    };
    
    /*! @brief Adds the right image (and eventually correct it).
     *
     * calls addRightImg(cv::Mat &ImgIn, bool correct)
     *
     * @param ImgIn The input image.
     * @param correct True to undistort the image.
     * @return The right image.
     */
    void addRightImg(cv::Mat &ImgIn, bool correct)
    {
        if (correct){
            // undistort
            cv::undistort(ImgIn, Img2, CM1, K1);
        }
        else
            Img2 = ImgIn;
    };
    
    /*! @brief Shows a figure.
     *
     * calls showFigure(const cv::Mat &imIn, const int &w, const int &h)
     *
     * @param imIn The input image.
     * @param w The desired image width in pixel.
     * @param h The desired image heigh in pixel.
     */
    void showFigure(const cv::Mat &imIn, const int &w, const int &h)
    {
        cv::Mat imOut;
        resize(imIn, imOut, cv::Size(w,h));
        imshow("Img", imOut);
        cv::waitKey(0);
    };
    
    /*! @brief Sets camera parameters.
     *
     * calls cameraParameters(const std::string & parameters_path)
     *
     * @param parameters_path Contains the path of the camera parameters
     *  (see sample .xml file in parameters folder).
     */
    void cameraParameters(const std::string & parameters_path)
    {
        cv::FileStorage f;
        f.open(parameters_path, cv::FileStorage::READ);
        
        f["Camera_Matrix_1"]        >> CM1;        // camera matrix 1
        f["Camera_Matrix_2"]        >> CM2;        // camera matrix 2
        f["Camera_Distortion_1"]    >> K1;         // distortion coefficient camera 1
        f["Camera_Distortion_2"]    >> K2;         // distortion coefficient camera 2
        f["R"]                      >> R12;        // rotation matrix from camera 1 to camera 2
        f["T"]                      >> T12;        // translation vector from camera 1 to camera 2
        f["imgSize"]                >> imgSize;    // input image size
        f.release();

        // Camera Matrix 1 inverse
        CM1Inv = Eigen::Matrix3d(CM1.ptr<double>(0));
        CM1Inv = CM1Inv.transpose().inverse();
        
        // Camera Matrix 2 inverse
        CM2Inv = Eigen::Matrix3d(CM2.ptr<double>(0));
        CM2Inv = CM2Inv.transpose().inverse();

        // prepare rotation and translation for triangulation
        rotMat12 = opengv::rotation_t(R12.ptr<double>(0)).transpose();
        trasl12 = opengv::translation_t(T12.ptr<double>(0)).transpose();
        
        // calculate projection matrices
        projectionMatrices(CM1, CM2, R12, T12, projMat1, projMat2);

    }

    /*! @enum Feature
     *
     * @brief This enum lists the different detector and descriptor supported by the VO class, that are SIFT, SURF, ORB, SURF on GPU, ORB on GPU.
     */
    enum Feature
    {
        SIFT_FEATURE,
        SURF_FEATURE,
        FAST_BRIEF_FEATURE,
//        ORB_FEATURE,
//        BRISK_FEATURE,
        SURF_GPU_FEATURE,
        ORB_GPU_FEATURE
    };

    /*! @enum algorithm
     *
     * @brief This enum lists the different motion estimation algorithms supported by the VO class, that are KNEIP, GAO, EPNP.
     */
    enum algorithm
    {
        RANSAC_KNEIP,
        RANSAC_GAO,
        RANSAC_EPNP
    };
    
    /*! @brief Computes the transformation matrix.
     *
     * calls computeRt(opengv::transformation_t &Rt, int algorithm, bool NonLinBool)
     *
     * @param Rt The transformation occured between two subsequent sets of images.
     * @param Algorithm The algorithm used to solve the motion estimation problem (see algorithm enum).
     * @param NonLinBool True for the non-linear optimization.
     */
    void computeRt(opengv::transformation_t &Rt, int algorithm, bool NonLinBool)
    {
        counter ++;
    
        std::cout << "------- Processing: Frame " << counter-1 << " -------" << std::endl << std::endl;
        if (counter ==1) {
            std::cout << " ...end" << std::endl << std::endl;
        }

        std::vector<float> des1;
        std::vector<float> des2;

        auto start = std::chrono::system_clock::now();
        
        // add
        int resizeCoeff = 2;
        cv::resize(Img1, Img1, cv::Size(Img1.cols/resizeCoeff,Img1.rows/resizeCoeff));
        cv::resize(Img2, Img2, cv::Size(Img2.cols/resizeCoeff,Img2.rows/resizeCoeff));
        
        // detect keypoints and compute descriptors
        switch (feature)
        {
            case SIFT_FEATURE:
                #ifdef VO_WITH_NONFREE
                featuresSift.detect(Img1, keypointsL);
                featuresSift.detect(Img2, keypointsR);
                descriptorSift.compute(Img1, keypointsL, descriptorL);
                descriptorSift.compute(Img2, keypointsR, descriptorR);
                outlierThreshold = 200.0;
                #endif
                break;
            case SURF_FEATURE:
                #ifdef VO_WITH_NONFREE
                detectorSurf.detect(Img1,keypointsL);
                detectorSurf.detect(Img2,keypointsR);
                extractorSurf.compute(Img1, keypointsL, descriptorL);
                extractorSurf.compute(Img2, keypointsR, descriptorR);
                outlierThreshold = 0.1;
                #endif
                break;
            case FAST_BRIEF_FEATURE:
                #ifdef VO_WITH_NONFREE
                featureFast.detect(Img1, keypointsL);
                featureFast.detect(Img2, keypointsR);
                descriptorBrief.compute(Img1, keypointsL, descriptorL);
                descriptorBrief.compute(Img2, keypointsR, descriptorR);
                outlierThreshold = 200.0;
                #endif
                break;
//            case ORB_FEATURE: // non va
//                #ifdef VO_WITH_NONFREE
//                featureOrb.detect(Img1, keypointsL);
//                featureOrb.detect(Img2, keypointsR);
//                featureOrb.compute(Img1, keypointsL, descriptorL);
//                featureOrb.compute(Img2, keypointsR, descriptorR);
//                outlierThreshold = 300.0;
//                #endif
//                break;
//            case BRISK_FEATURE: // non va
//                #ifdef VO_WITH_NONFREE
//                featureBrisk.detect(Img1, keypointsL);
//                featureBrisk.detect(Img2, keypointsR);
//                featureBrisk.compute(Img1, keypointsL, descriptorL);
//                featureBrisk.compute(Img2, keypointsR, descriptorR);
//                outlierThreshold = 300.0;
//                #endif
//                break;
            case SURF_GPU_FEATURE:
                #ifdef VO_WITH_CUDA
                // Load images into GPU
                ImgL_g.upload(Img1);
                ImgR_g.upload(Img2);
                featuresSurfGPU(ImgL_g, cv::cuda::GpuMat(), keypointsL_g, descriptorL_g, false);
                featuresSurfGPU(ImgR_g, cv::cuda::GpuMat(), keypointsR_g, descriptorR_g, false);
                
                cudaMatcher->match(descriptorL_g, descriptorR_g, matches);
                
                featuresSurfGPU.downloadKeypoints(keypointsL_g, keypointsL);
                featuresSurfGPU.downloadKeypoints(keypointsR_g, keypointsR);
                featuresSurfGPU.downloadDescriptors(descriptorL_g, des1);
                featuresSurfGPU.downloadDescriptors(descriptorR_g, des2);
                
                descriptorL = cv::Mat(des1.size()/64,64,CV_32FC1);
                memcpy(descriptorL.data, des1.data(), des1.size()*sizeof(float));

                descriptorR = cv::Mat(des2.size()/64,64,CV_32FC1);
                memcpy(descriptorR.data, des2.data(), des2.size()*sizeof(float));
                
                outlierThreshold = 0.1;
                #endif
                break;
            default:
                break;
        };
        
        
        // feature matching (left-right)
        if(feature != SURF_GPU_FEATURE)
            matcher.match(descriptorL, descriptorR, matches);
        
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = end-start;
        std::cout << "Image processing time (detector+descriptor+matcher): " << elapsed.count() << " s" << std::endl << std::endl;
        
        // add
        for (int i=0; i<keypointsL.size(); i++)
        {
            keypointsL[i].pt.x = keypointsL[i].pt.x*resizeCoeff;
            keypointsL[i].pt.y = keypointsL[i].pt.y*resizeCoeff;
            keypointsR[i].pt.x = keypointsR[i].pt.x*resizeCoeff;
            keypointsR[i].pt.y = keypointsR[i].pt.y*resizeCoeff;
        }

        
        ////////////////////////////////////////////////////////////////////////////
        
        std::stringstream ss;
        
        if (counter > 1)
        {

            // feature tracking (between left camera keypoints at time t and left camera keypoints at time t+dt)
            if(feature == SURF_GPU_FEATURE)
            {
                #ifdef VO_WITH_CUDA
                cv::cuda::GpuMat descriptorOld_g;
                cv::cuda::GpuMat descriptorL_g;
                descriptorOld_g.upload(descriptorOld);
                descriptorL_g.upload(descriptorL);

                cudaMatcher->match(descriptorOld_g, descriptorL_g, matchesDt);
                #endif
            }
            else
                matcher.match(descriptorOld, descriptorL, matchesDt);
            
//            drawBestMatches(matchesDt, ImgOld, Img1, keypointsOld, keypointsL, 20, counter, 2, "/Users/giuliasolda/Desktop/VO_software/Images/ORB_matches/");
            
            // considering all the previous 3D points...
            size_t numberPoints = std::min(points3DOld.size(), matches.size());
            for (int i=0; i<numberPoints; i++)
            {
                // store the 3D point
                points.push_back(points3DOld[i]);
                
                // reproject the 3D point in 2D left camera plane
                point3DHom = cv::Mat(4,1, CV_64F);
                for (int p=0; p<3; p++){
                    point3DHom.at<double>(p,0) = points3DOld[i](p);
                }
                point3DHom.at<double>(3,0) = 1;
                point3DRiprojHom = projMat1*point3DHom;
                point3DRiproj = cv::Point2f(point3DRiprojHom.at<double>(0,0)/point3DRiprojHom.at<double>(2,0), point3DRiprojHom.at<double>(1,0)/point3DRiprojHom.at<double>(2,0));
                point3DRiprojVec(0) = point3DRiprojHom.at<double>(0,0)/point3DRiprojHom.at<double>(2,0);
                point3DRiprojVec(1) = point3DRiprojHom.at<double>(1,0)/point3DRiprojHom.at<double>(2,0);
                points3DRiprojVector.push_back(point3DRiprojVec);

                // create the vector of points 2D (left actual image) in homogeneous coordinates, sorted as the 3D points at previous time
                point2DLeftHom(0) = keypointsL[matchesDt[matchesOld[i].queryIdx].trainIdx].pt.x;
                point2DLeftHom(1) = keypointsL[matchesDt[matchesOld[i].queryIdx].trainIdx].pt.y;
                point2DLeftHom(2) = 1.;
                points2DLeftHomVector.push_back(point2DLeftHom);
                
                // opencv 2D left points
                point2DLeft = cv::Point2f(point2DLeftHom(0), point2DLeftHom(1));
                points2DLeft.push_back(point2DLeft);
                
                // create the bearing vectors (from the left camera to the 2D left points)
                bearingVectorLeft = CM1Inv*point2DLeftHom;
                bearingVectorLeft/= bearingVectorLeft.norm();
                bearingVectorsLeft.push_back(bearingVectorLeft);

                // create the vector of points 2D (right actual image) in homogeneous coordinate, sorted as the 3D points at previous time
                int keypointLeftNewIndex = matchesDt[matchesOld[i].queryIdx].trainIdx;
                point2DRightHom(0) = keypointsR[matches[keypointLeftNewIndex].trainIdx].pt.x;
                point2DRightHom(1) = keypointsR[matches[keypointLeftNewIndex].trainIdx].pt.y;
                point2DRightHom(2) = 1.;
                points2DRightHomVector.push_back(point2DRightHom);
                
                // create the bearing vectors (from the right camera to the 2D right points)
                bearingVectorRight = CM2Inv*point2DRightHom;
                bearingVectorRight/= bearingVectorRight.norm();
                bearingVectorsRight.push_back(bearingVectorRight);
                
            }

            
            // choose the corrispondences for Ransac (all the elements)
            opengv::bearingVectors_t bearingVectorsForRansac = bearingVectorsLeft;
            opengv::points_t pointsForRansac = points;
     
            // set Ransac parameters
            double treshold = 1.0 - cos(atan(2.0/CM1.at<double>(0,0)));
            

            // create a central absolute adapter (OpenGV class)
            opengv::absolute_pose::CentralAbsoluteAdapter adapter(bearingVectorsForRansac, pointsForRansac);
            
            // create an AbsolutePoseSacProblem (algorithm: KNEIP)
            // (algorithm is selectable: KNEIP, GAO, or EPNP)
            switch (algorithm) {
                case 0:
                    algorithmType = opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem::KNEIP;
                    break;
                case 1:
                    algorithmType = opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem::GAO;
                    break;
                case 2:
                    algorithmType = opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem::EPNP;
                    break;
                default:
                    break;
            }
            

            // create a Ransac object
            opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> ransac;
            
    
            boost::shared_ptr<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem>
            absposeproblem(new opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem(adapter, algorithmType));
            
            std::string name = "Current pose (Ransac): ";
            auto start = std::chrono::system_clock::now();
            
            runRansac(ransac, adapter, absposeproblem, treshold, 10000, Rt_Ransac);
            
            auto end = std::chrono::system_clock::now();
            std::chrono::duration<double> elapsed = end-start;
            
            std::cout << "| Ransac parameters" << std::endl << "| Iterations needed: " << ransac.iterations_ << "/" << ransac.max_iterations_ << std::endl << "| Number of inliers: " << ransac.inliers_.size() << std::endl << "| Time needed: " << elapsed.count() << " s" << std::endl <<  std::endl;
            
            // print the results
//            std::cout << name << std::endl;
//            std::cout << Rt_Ransac << std::endl << std::endl;
 
            if (NonLinBool)
            {
                std::string nameNonLin = "Current pose (non-linear optimization): ";
                
                nonLinearOptimization(ransac, adapter, Rt_Ransac, Rt_NonLin);
                Rt = Rt_NonLin;
                
                // print the result
                std::cout << nameNonLin << std::endl;
//                std::cout << Rt << std::endl << std::endl;
            }
            else
                Rt = Rt_Ransac;
            
            
    
        }
        
    
        ////////////////////////////////////////////////////////////////////////////
        
        
        // sort matches (best score)
        std::sort(matches.begin(), matches.end(), compareMatch);

//        std::cout << "MATCHES NUMBER: " << matches.size() << std::endl;
        
//        drawBestMatches(matches, Img1, Img2, keypointsL, keypointsR, 20, counter, 2, "/Users/giuliasolda/Desktop/VO_software/Images/ORB_matches/");
        
        // eliminate matches with score too low (distance > 0.1)
        int ind_match = 0;
        
        while (matches[ind_match].distance < outlierThreshold && ind_match < matches.size())
        {
            ind_match++;
        }
        matches.resize(ind_match);
        
        
        for (int m=0; m<matches.size(); m++)
        {
            // bearing vectors with left keypoints
            point2DLeftHomSort(0) = keypointsL[matches[m].queryIdx].pt.x;
            point2DLeftHomSort(1) = keypointsL[matches[m].queryIdx].pt.y;
            point2DLeftHomSort(2) = 1.;
            points2DLeftHomVectorOld.push_back(point2DLeftHomSort);
            bearingVectorLeftSorted = CM1Inv*point2DLeftHomSort;
            bearingVectorLeftSorted/= bearingVectorLeftSorted.norm();
            bearingVectorsLeftSorted.push_back(bearingVectorLeftSorted);
            
            // bearing vectors with right keypoints
            point2DRightHomSort(0) = keypointsR[matches[m].trainIdx].pt.x;
            point2DRightHomSort(1) = keypointsR[matches[m].trainIdx].pt.y;
            point2DRightHomSort(2) = 1.;
            bearingVectorRightSorted = CM2Inv*point2DRightHomSort;
            bearingVectorRightSorted/= bearingVectorRightSorted.norm();
            bearingVectorsRightSorted.push_back(bearingVectorRightSorted);

        };
        
        
        // create a central relative adapter
        opengv::relative_pose::CentralRelativeAdapter adapterTriangulation(bearingVectorsLeftSorted, bearingVectorsRightSorted, trasl12, rotMat12);
        
        int q = 0;
        
        for (int t=0; t<bearingVectorsLeftSorted.size(); t++)
        {
            // triangulate points
            opengv::point_t pointTriangulate = opengv::triangulation::triangulate(adapterTriangulation, t);
            
        
            // point 3D reprojection
            cv::Mat pointTriangulateHom = cv::Mat(4,1, CV_64F);
            for (int p=0; p<3; p++){
                pointTriangulateHom.at<double>(p,0) = pointTriangulate(p);
            }
            pointTriangulateHom.at<double>(3,0) = 1;
            
            cv::Mat pointTriangulateRiprojHom = projMat1*pointTriangulateHom;
            cv::Point2f pointTriangulateRiproj = cv::Point2f(pointTriangulateRiprojHom.at<double>(0,0)/pointTriangulateRiprojHom.at<double>(2,0), pointTriangulateRiprojHom.at<double>(1,0)/pointTriangulateRiprojHom.at<double>(2,0));
            
        
            // corrispondent 2D point
            cv::Point2f point2D = cv::Point2f(points2DLeftHomVectorOld[t](0)/points2DLeftHomVectorOld[t](2), points2DLeftHomVectorOld[t](1)/points2DLeftHomVectorOld[t](2));

            
            double distance2D3D = cv::norm(pointTriangulateRiproj-point2D);
            
            
            if (distance2D3D >= 2)
            {
                matches.erase(matches.begin()+q);
            }
            else
            {
                points3DTriangulate.push_back(pointTriangulate);
                q++;
            }
        
    
            

        }
        
//        std::cout << "INLIERS NUMBER: " << matches.size() << std::endl;
        
        // prepare the elements for the next iteration
        points3DOld = points3DTriangulate;
        matchesOld = matches;
        keypointsOld = keypointsL;
        descriptorOld = descriptorL;
        Img1.copyTo(ImgOld);
        
        points3DTriangulate.clear();
        bearingVectorsLeftSorted.clear();
        bearingVectorsRightSorted.clear();
        bearingVectorsLeft.clear();
        bearingVectorsRight.clear();
        points.clear();
        points2DLeftHomVector.clear();
        points2DRightHomVector.clear();
        points2DLeftHomVectorOld.clear();
        points3DRiprojVector.clear();
  
    };
    
    
private:
    
    // matches sorting
    static bool compareMatch(const cv::DMatch &a, const cv::DMatch &b)
    {
        return a.distance<b.distance;
    }
    
    int counter = 0;
    
    cv::Mat Img1, Img2, Img1Und, Img2Und, ImgOld;
    cv::Mat CM1, CM2, K1, K2, R12, T12, imgSize;
    cv::Mat RtCam1, RtCam2;
    cv::Mat projMat1, projMat2;
    Eigen::Matrix3d CM1Inv, CM2Inv;
    
    // detector
    int feature;
#ifdef VO_WITH_NONFREE
    cv::SIFT featuresSift;
    cv::SurfFeatureDetector detectorSurf;
    cv::FastFeatureDetector featureFast;
    cv::ORB featureOrb;
    cv::BRISK featureBrisk;
    cv::Ptr<cv::DescriptorMatcher> orbMatcher;
#endif
#ifdef VO_WITH_CUDA
    cv::cuda::GpuMat ImgL_g, ImgR_g;
    cv::cuda::GpuMat keypointsL_g, keypointsR_g, descriptorL_g, descriptorR_g;
    cv::cuda::SURF_CUDA featuresSurfGPU;
    cv::Ptr<cv::cuda::DescriptorMatcher> cudaMatcher;
#endif

    std::vector<cv::KeyPoint> keypointsL, keypointsR, keypointsOld;
    
    // descriptor
    cv::Mat descriptorL, descriptorR;
    cv::Mat descriptorOld;
#ifdef VO_WITH_NONFREE
    cv::SiftDescriptorExtractor descriptorSift;
    cv::BriefDescriptorExtractor descriptorBrief;
    cv::SurfDescriptorExtractor extractorSurf;
#endif

    // matching
    cv::BFMatcher matcher;
    std::vector <cv::DMatch> matches;
    std::vector <cv::DMatch> matchesDt;
    std::vector <cv::DMatch> matchesOld;
    
    Eigen::Vector3d point2DLeftHom;
    Eigen::Vector3d point2DRightHom;
    Eigen::Vector3d point2DLeftHomSort;
    Eigen::Vector3d point2DRightHomSort;
    Eigen::Vector3d point3DRiprojVec;
    cv::Mat point3DHom;
    cv::Mat point3DRiprojHom;
    cv::Point2f point3DRiproj;
    cv::Point2f point2DLeft;
    
    opengv::bearingVector_t bearingVectorLeft;
    opengv::bearingVector_t bearingVectorRight;
    opengv::bearingVector_t bearingVectorLeftSorted;
    opengv::bearingVector_t bearingVectorRightSorted;
    opengv::bearingVectors_t bearingVectorsLeft;
    opengv::bearingVectors_t bearingVectorsRight;
    opengv::bearingVectors_t bearingVectorsLeftSorted;
    opengv::bearingVectors_t bearingVectorsRightSorted;
    opengv::points_t points;
    
    opengv::translation_t initialTranslation;
    opengv::rotation_t initialRotation;
    
    opengv::translation_t trasl12;
    opengv::rotation_t rotMat12;
    opengv::point_t pointTriangulate;
    std::vector<opengv::point_t> points3DTriangulate;
    std::vector<opengv::point_t> points3DOld;

    std::vector <Eigen::Vector3d> points2DLeftHomVector;
    std::vector <Eigen::Vector3d> points2DRightHomVector;
    std::vector <Eigen::Vector3d> points2DLeftHomVectorOld;
    std::vector <Eigen::Vector3d> points3DRiprojVector;
    std::vector <cv::Point2f> points2DLeft;
    
    opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem::Algorithm algorithmType;
    opengv::transformation_t Rt_Ransac;
    opengv::transformation_t Rt_NonLin;
    
    double outlierThreshold;
    
    /*! @brief Calculate the projection matrices.
     *
     * calls projectionMatrices(cv::Mat &CM1, cv::Mat &CM2, cv::Mat &R12, cv::Mat &T12,
     cv::Mat &projMat1, cv::Mat &projMat2)
     *
     * @param CM1 The left camera matrix.
     * @param CM2 The right camera matrix.
     * @param R12 The rotation from the left camera back to the right camera frame.
     * @param T12 The translation from the left camera back to the right camera frame.
     * @param projMat1 The output left camera projection matrix.
     * @param projMat2 The output right camera projection matrix.
     */
    void projectionMatrices(cv::Mat &CM1, cv::Mat &CM2, cv::Mat &R12, cv::Mat &T12,
                            cv::Mat &projMat1, cv::Mat &projMat2)
    {
        RtCam1 = cv::Mat::eye(3, 4, CV_64F);
        hconcat(R12, T12, RtCam2);
        cv::Mat lastRow = cv::Mat::zeros(1,4, CV_64F);
        lastRow.at<double>(0,3) = 1;
        cv::Mat RtCam1_4x4, RtCam2_4x4;
        cv::Mat RtCam1Inv_4x4, RtCam2Inv_4x4;
        cv::Mat RtCam1Inv(3,4,CV_64F);
        cv::Mat RtCam2Inv(3,4,CV_64F);
        vconcat(RtCam1, lastRow, RtCam1_4x4);
        vconcat(RtCam2, lastRow, RtCam2_4x4);
        RtCam1Inv_4x4 = RtCam1_4x4.inv();
        RtCam2Inv_4x4 = RtCam2_4x4.inv();
        for (int r=0; r<3; r++) {
            for (int c=0; c<4; c++) {
                RtCam1Inv.at<double>(r,c) = RtCam1Inv_4x4.at<double>(r,c);
                RtCam2Inv.at<double>(r,c) = RtCam2Inv_4x4.at<double>(r,c);
            }
        };
        projMat1 = CM1*RtCam1Inv;
        projMat2 = CM2*RtCam2Inv;
    };
    
    /*! @brief Solve a RANSAC problem.
     *
     * calls runRansac(opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, opengv::absolute_pose::CentralAbsoluteAdapter &adapter, boost::shared_ptr<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &absposeproblem, double &treshold, int maxIterations, opengv::transformation_t &Rt, std::string &name)
     *
     * @param ransac A Ransac object (see OpenGV documentation http://laurentkneip.github.io/opengv/).
     * @param adapter A central absolute adapter (see openGV documentation http://laurentkneip.github.io/opengv/).
     * @param absposeproblem An absolute pose problem object (see OpenGV documentation http://laurentkneip.github.io/opengv/).
     * @param treshold The Ransac threshold, defined as $\epsilon_{threshold} = 1-\cos{\left(\arctan{\left(\psi/l\right)}\right)}.
     * @param maxIterations The Ransac maximum number of iterations.
     * @param Rt The output transformation.
     */
    void runRansac(opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, opengv::absolute_pose::CentralAbsoluteAdapter &adapter, boost::shared_ptr<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &absposeproblem, double &treshold, int maxIterations, opengv::transformation_t &Rt)
    {
        // run ransac
        ransac.sac_model_ = absposeproblem;
        ransac.threshold_ = treshold;
        ransac.max_iterations_ = maxIterations;
        ransac.computeModel();
        
        // get the result
        if (ransac.iterations_>= maxIterations)
        {
            Rt = opengv::transformation_t();
            Rt(0,0) = 1;
            Rt(1,1) = 1;
            Rt(2,2) = 1;
            std::cout << std::endl << "WARNING: Ransac iterations exceed the limit. Result is not reliable." << std::endl << std::endl;
        }
        else
            Rt = ransac.model_coefficients_;
    };
    
    /*! @brief Apply a non-linear optimization problem.
     *
     * calls nonLinearOptimization(opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, opengv::bearingVectors_t bearingVectorsForRansac, opengv::points_t pointsForRansac, opengv::transformation_t &Rt, opengv::transformation_t &Rt_NonLin, std::string &name)
     *
     * @param ransac The Ransac object.
     * @param adapterRansac The Ransac central absolute adapter.
     * @param Rt The initial transformation.
     * @param Rt_NonLin The output transformation.
     */
    void nonLinearOptimization(opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac,  opengv::absolute_pose::CentralAbsoluteAdapter &adapterRansac, opengv::transformation_t &Rt, opengv::transformation_t &Rt_NonLin)
    {
        opengv::bearingVectors_t bearingVectorsExitRansac;
        opengv::points_t pointsExitRansac;
        
        int numberInliers = ransac.inliers_.size();
        for(size_t i=0; i<numberInliers; i++)
        {
            int index = ransac.inliers_[i];
            bearingVectorsExitRansac.push_back(adapterRansac.getBearingVector(index));
            pointsExitRansac.push_back(adapterRansac.getPoint(index));
        }
        opengv::absolute_pose::CentralAbsoluteAdapter adapter(bearingVectorsExitRansac, pointsExitRansac);
        // set the initial translation and rotation
        initialTranslation = Rt.col(3);
        initialRotation.col(0) = Rt.col(0);
        initialRotation.col(1) = Rt.col(1);
        initialRotation.col(2) = Rt.col(2);
        adapter.sett(initialTranslation);
        adapter.setR(initialRotation);
        
        // non-linear optimization
        Rt_NonLin = opengv::absolute_pose::optimize_nonlinear(adapter);
        
        bearingVectorsExitRansac.clear();
        pointsExitRansac.clear();
    };



    
};


#endif // VISUAL_ODOMETRY_H
