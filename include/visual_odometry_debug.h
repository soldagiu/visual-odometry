#ifndef VISUAL_ODOMETRY_H
#define VISUAL_ODOMETRY_H

#include <iostream>
#include "debug_methods.h"
#include <vector>
#include <string>
#include <fstream>
#include <algorithm>
#include <sstream>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <opengv/absolute_pose/methods.hpp>
#include <opengv/absolute_pose/CentralAbsoluteAdapter.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/triangulation/methods.hpp>
#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/absolute_pose/AbsolutePoseSacProblem.hpp>

#include <chrono>
#include <unistd.h>


class visualOdometry
{
public:
    
    // class constructor
    visualOdometry(int feat = 0)
    {
        feature = feat;
        
        if (feature==SIFT_FEATURE)
        {
            featuresSift = cv::SIFT(0, 3, 0.08);
        }
        if (feature==SURF_FEATURE)
        {
            detectorSurf = cv::SurfFeatureDetector(500);
        }
        if (feature==ORB_FEATURE)
        {
            featureOrb = cv::ORB(500);
        }
    };
    
    // add Left image (and eventually correct it)
    void addLeftImg(cv::Mat &LeftImg, bool correct)
    {
        if (correct){
            // demosaicing
            // the input image must have 1 channel, while the output one must have 3
            cv::Mat ImL3;
            cvtColor(LeftImg, ImL3, CV_BayerGR2BGR);
            // flip images
            flip(ImL3, ImL3, -1);
            // undistort
            cv::undistort(ImL3, LeftImg, CM1, K1);
        }
        Img1 = LeftImg;
    };
    
    // add right image (and eventually correct it)
    void addRightImg(cv::Mat &RightImg, bool correct)
    {
        if (correct){
            // demosaicing
            // the input image must have 1 channel, while the output one must have 3
            cv::Mat ImR3;
            cvtColor(RightImg, ImR3, CV_BayerGR2BGR);
            // flip images
            flip(ImR3, ImR3, -1);
            // undistort
            cv::undistort(ImR3, RightImg, CM1, K1);
        }
        Img2 = RightImg;
    };
    
    // show figure (input: width and heigh)
    void showFigure(const cv::Mat &imIn, const int &w, const int &h)
    {
        cv::Mat imOut;
        resize(imIn, imOut, cv::Size(w,h));
        imshow("Img", imOut);
        cv::waitKey(0);
    };
    
    // calculate the projection matrices
    void projectionMatrices(cv::Mat &CM1, cv::Mat &CM2, cv::Mat &R12, cv::Mat &T12, cv::Mat &projMat1, cv::Mat &projMat2)
    {
        RtCam1 = cv::Mat::eye(3, 4, CV_64F);
        hconcat(R12, T12, RtCam2);
        cv::Mat lastRow = cv::Mat::zeros(1,4, CV_64F);
        lastRow.at<double>(0,3) = 1;
        cv::Mat RtCam1_4x4, RtCam2_4x4;
        cv::Mat RtCam1Inv_4x4, RtCam2Inv_4x4;
        cv::Mat RtCam1Inv(3,4,CV_64F);
        cv::Mat RtCam2Inv(3,4,CV_64F);
        vconcat(RtCam1, lastRow, RtCam1_4x4);
        vconcat(RtCam2, lastRow, RtCam2_4x4);
        RtCam1Inv_4x4 = RtCam1_4x4.inv();
        RtCam2Inv_4x4 = RtCam2_4x4.inv();
        for (int r=0; r<3; r++) {
            for (int c=0; c<4; c++) {
                RtCam1Inv.at<double>(r,c) = RtCam1Inv_4x4.at<double>(r,c);
                RtCam2Inv.at<double>(r,c) = RtCam2Inv_4x4.at<double>(r,c);
            }
        };
        projMat1 = CM1*RtCam1Inv;
        projMat2 = CM2*RtCam2Inv;
    };
    
    // set camera parameters
    void cameraParameters(const std::string & parameters_path)
    {
        cv::FileStorage f;
        f.open(parameters_path, cv::FileStorage::READ);
        
        f["Camera_Matrix_1"]        >> CM1;        // camera matrix 1
        f["Camera_Matrix_2"]        >> CM2;        // camera matrix 2
        f["Camera_Distortion_1"]    >> K1;         // distortion coefficient camera 1
        f["Camera_Distortion_2"]    >> K2;         // distortion coefficient camera 2
        f["R"]                      >> R12;        // rotation matrix from camera 1 to camera 2
        f["T"]                      >> T12;        // translation vector from camera 1 to camera 2
        f["imgSize"]                >> imgSize;    // input image size
        f.release();
        
        // Camera Matrix 1 inverse
        CM1Inv = Eigen::Matrix3d(CM1.ptr<double>(0));
        CM1Inv = CM1Inv.transpose().inverse();
        
        // Camera Matrix 2 inverse
        CM2Inv = Eigen::Matrix3d(CM2.ptr<double>(0));
        CM2Inv = CM2Inv.transpose().inverse();
        
        // prepare rotation and translation for triangulation
        rotMat12 = opengv::rotation_t(R12.ptr<double>(0)).transpose();
        trasl12 = opengv::translation_t(T12.ptr<double>(0)).transpose();
        
        // calculate projection matrices
        projectionMatrices(CM1, CM2, R12, T12, projMat1, projMat2);
        
        
    }
    
    // solve a Ransac problem
    void runRansac(opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, opengv::absolute_pose::CentralAbsoluteAdapter &adapter, boost::shared_ptr<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &absposeproblem, double &treshold, int maxIterations, opengv::transformation_t &Rt, std::string &name, opengv::transformations_t &RtForMatlab)
    {
        
        // run ransac
        ransac.sac_model_ = absposeproblem;
        ransac.threshold_ = treshold;
        ransac.max_iterations_ = maxIterations;
        ransac.computeModel();
        auto start = std::chrono::system_clock::now();
        // get the result
        Rt = ransac.model_coefficients_;
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = end-start;
        
        // print the resulta
        std::cout << name << std::endl;
        std::cout << Rt << std::endl << std::endl;
        std::cout << "| Iterations needed: " << ransac.iterations_ << "/" << ransac.max_iterations_ << std::endl << "| Number of inliers: " << ransac.inliers_.size() << std::endl << "| Time needed: " << elapsed.count() << " s" << std::endl <<  std::endl;
        
        // create the vector of transformations (Matlab debug)
        RtForMatlab.push_back(Rt);
    };
    
    // Non-linear optimization
    void nonLinearOptimization(opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, opengv::bearingVectors_t bearingVectorsForRansac, opengv::points_t pointsForRansac, opengv::transformation_t &Rt, opengv::transformation_t &Rt_NonLin, std::string &name, opengv::transformations_t &RtForMatlab)
    {
        opengv::bearingVectors_t bearingVectorsExitRansac;
        opengv::points_t pointsExitRansac;
        
        int numberInliers = ransac.inliers_.size();
        for(size_t i=0; i<numberInliers; i++)
        {
            int index = ransac.inliers_[i];
            bearingVectorsExitRansac.push_back(bearingVectorsForRansac[index]);
            pointsExitRansac.push_back(pointsForRansac[index]);
        }
        opengv::absolute_pose::CentralAbsoluteAdapter adapter(bearingVectorsExitRansac, pointsExitRansac);
        // set the initial translation and rotation
        initialTranslation = Rt.col(3);
        initialRotation.col(0) = Rt.col(0);
        initialRotation.col(1) = Rt.col(1);
        initialRotation.col(2) = Rt.col(2);
        adapter.sett(initialTranslation);
        adapter.setR(initialRotation);
        
        // non-linear optimization
        Rt_NonLin = opengv::absolute_pose::optimize_nonlinear(adapter);
        
        // print the result
        std::cout << name << std::endl;
        std::cout << Rt_NonLin << std::endl << std::endl;
        
        // create the vector of transformations (Matlab debug)
        RtForMatlab.push_back(Rt_NonLin);
        
        bearingVectorsExitRansac.clear();
        pointsExitRansac.clear();
    };
    
    
    enum Feature
    {
        SIFT_FEATURE,
        SURF_FEATURE,
        ORB_FEATURE,
        SURF_GPU_FEATURE,
        ORB_GPU_FEATURE
    };
    
    enum algorithm
    {
        RANSAC_KNEIP,
        RANSAC_GAO,
        RANSAC_EPNP
    };

    
    // calculate the transformation
    void computeRt(opengv::transformation_t &Rt, int algorithm)
    {
        counter ++;
        
        
        // plot difference (1 for undistortion)
        printDifference(CM1, K1, 0, 2);
        
        
        std::cout << "------- Processing: Frame " << counter-1 << " -------" << std::endl << std::endl;
        if (counter ==1) {
            std::cout << " ...end" << std::endl << std::endl;
        }
        
        // detecting keypoints
        switch (feature)
        {
            case SIFT_FEATURE:
                featuresSift.detect(Img1, keypointsL);
                featuresSift.detect(Img2, keypointsR);
                descriptorSift.compute(Img1, keypointsL, descriptorL);
                descriptorSift.compute(Img2, keypointsR, descriptorR);
                outlierThreshold = 200.0;
                break;
            case SURF_FEATURE:
                detectorSurf.detect(Img1,keypointsL);
                detectorSurf.detect(Img2,keypointsR);
                extractorSurf.compute(Img1, keypointsL, descriptorL);
                extractorSurf.compute(Img2, keypointsR, descriptorR);
                outlierThreshold = 0.1;
                break;
            case ORB_FEATURE:
                //                featureFast.detect(Img1, keypointsL);
                //                featureFast.detect(Img2, keypointsR);
                //                descriptorBrief.compute(Img1, keypointsL, descriptorL);
                //                descriptorBrief.compute(Img2, keypointsR, descriptorR);
                featureOrb.detect(Img1, keypointsL);
                featureOrb.detect(Img2, keypointsR);
                featureOrb.compute(Img1, keypointsL, descriptorL);
                featureOrb.compute(Img2, keypointsR, descriptorR);
                outlierThreshold = 200.0;
                break;
            default:
                break;
        };

        
        // feature matching (left-right)
        matcher.match(descriptorL, descriptorR, matches);
        
//        std::cout << "matches: " << matches.size() << std::endl;
        
        //        if(counter!=1){
        //        std::cout << ", Matches: " << matches.size();
        //        }
        
        
        ////////////////////////////////////////////////////////////////////////////
        
        std::stringstream ss;
        if (counter > 1)
        {
            
            // feature tracking (between left camera keypoints at time t and left camera keypoints at time t+dt)
            matcher.match(descriptorOld, descriptorL, matchesDt);
            
//            // draw features tracking (debug)
//            std::sort(matchesDt.begin(), matchesDt.end(), compareMatch);
//            drawTrackedMatches(matchesDt, keypointsOld, keypointsL, ImgOld, Img1, counter, 20, 2);
            
            // considering all the previous 3D points...
            size_t numberPoints = std::min(points3DOld.size(), matches.size());
            for (int i=0; i<numberPoints; i++)
            {
                // store the 3D point
                points.push_back(points3DOld[i]);
                
                // reproject the 3D point in 2D left camera plane
                point3DHom = cv::Mat(4,1, CV_64F);
                for (int p=0; p<3; p++){
                    point3DHom.at<double>(p,0) = points3DOld[i](p);
                }
                point3DHom.at<double>(3,0) = 1;
                point3DRiprojHom = projMat1*point3DHom;
                point3DRiproj = cv::Point2f(point3DRiprojHom.at<double>(0,0)/point3DRiprojHom.at<double>(2,0), point3DRiprojHom.at<double>(1,0)/point3DRiprojHom.at<double>(2,0));
                point3DRiprojVec(0) = point3DRiprojHom.at<double>(0,0)/point3DRiprojHom.at<double>(2,0);
                point3DRiprojVec(1) = point3DRiprojHom.at<double>(1,0)/point3DRiprojHom.at<double>(2,0);
                points3DRiprojVector.push_back(point3DRiprojVec);
                
                // create the vector of points 2D (left actual image) in homogeneous coordinates, sorted as the 3D points at previous time
                point2DLeftHom(0) = keypointsL[matchesDt[matchesOld[i].queryIdx].trainIdx].pt.x;
                point2DLeftHom(1) = keypointsL[matchesDt[matchesOld[i].queryIdx].trainIdx].pt.y;
                point2DLeftHom(2) = 1.;
                points2DLeftHomVector.push_back(point2DLeftHom);
                
                // opencv 2D left points
                point2DLeft = cv::Point2f(point2DLeftHom(0), point2DLeftHom(1));
                points2DLeft.push_back(point2DLeft);
                
                // create the bearing vectors (from the left camera to the 2D left points)
                bearingVectorLeft = CM1Inv*point2DLeftHom;
                bearingVectorLeft/= bearingVectorLeft.norm();
                bearingVectorsLeft.push_back(bearingVectorLeft);
                
                // create the vector of points 2D (right actual image) in homogeneous coordinate, sorted as the 3D points at previous time
                int keypointLeftNewIndex = matchesDt[matchesOld[i].queryIdx].trainIdx;
                point2DRightHom(0) = keypointsR[matches[keypointLeftNewIndex].trainIdx].pt.x;
                point2DRightHom(1) = keypointsR[matches[keypointLeftNewIndex].trainIdx].pt.y;
                point2DRightHom(2) = 1.;
                points2DRightHomVector.push_back(point2DRightHom);
                
                // create the bearing vectors (from the right camera to the 2D right points)
                bearingVectorRight = CM2Inv*point2DRightHom;
                bearingVectorRight/= bearingVectorRight.norm();
                bearingVectorsRight.push_back(bearingVectorRight);
                
//                // distance between the 3D reprojected point and the 2D (in the left plane)
//                double distance2D3Driproj;
//                distance2D3Driproj = cv::norm(point3DRiproj-point2DLeft);
//                
//                std::cout << distance2D3Driproj << std::endl;
//                // create the vectors without the outlier elements
//                if (distance2D3Driproj < 10)
//                {
//                    // store only the correct points
//                    pointsWithoutOutliers.push_back(points3DOld[i]);
//                    points2DLeftHomVectorWithoutOutliers.push_back(point2DLeftHom);
//                    bearingVectorsLeftWithoutOutliers.push_back(bearingVectorLeft);
//                };
                
            }
            
            
//            // draw 2D-3D corrispondences without outliers (debug)
//            points2D3Dcorrispondences(Img1, projMat1, points2DLeftHomVectorWithoutOutliers, pointsWithoutOutliers, 20, counter, 2);
            
            
            
        
            
            
            //////////////////////////////////////////////
            ////               OPENGV               //////
            //////////////////////////////////////////////
            
            // choose the corrispondences for Ransac (all the elements)
            opengv::bearingVectors_t bearingVectorsForRansac = bearingVectorsLeft;
            opengv::points_t pointsForRansac = points;
            
            // set Ransac parameters
            double treshold = 1.0 - cos(atan(2.0/1133.0));
            
            //            // number of 2D-3D corrispondences
            //            if(counter!=1){
            //                std::cout<< std::endl << std::endl << "2D-3D corrispondences: " << pointsForRansac.size();
            //            }
            
            
            //            // create a vector of bearing vector and a vector of the corresponding 3D points (at previous time)
            //            bearingVectorsStore(bearingVectorsForRansac, bearingVectorsForMatlab);
            //            pointsStore(pointsForRansac, pointsForMatlab);
            
            
            // create a central absolute adapter (opengv class)
            opengv::absolute_pose::CentralAbsoluteAdapter adapter(bearingVectorsForRansac, pointsForRansac);
            
            
            
            //            cv::Mat foto = cv::imread("/Users/giuliasolda/Desktop/terreno.jpg");
            //            std::vector <int> compr;
            //            compr.push_back(CV_IMWRITE_PNG_COMPRESSION);
            //            compr.push_back(9);
            //            auto start = std::chrono::system_clock::now();
            //            cv::imwrite("/Users/giuliasolda/Desktop/terreno.png", foto, compr);
            //            auto end = std::chrono::system_clock::now();
            //
            //            std::chrono::duration<double> elapsed = end-start;
            //            std::cout << elapsed.count() << std::endl;
            
            ///////////////////////////////////////////// KNEIP ///////////////////////////////////////////////////
            
            // create a Ransac object
            opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> ransacKneip;
            
            // create an AbsolutePoseSacProblem (algorithm: KNEIP)
            // (algorithm is selectable: KNEIP, GAO, or EPNP)
            boost::shared_ptr<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem>
            absposeproblem_Kneip(new opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem(adapter, opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem::KNEIP));
            
            std::string nameKneip = "Current pose (Ransac - Kneip algorithm): ";
            
            
            runRansac(ransacKneip, adapter, absposeproblem_Kneip, treshold, 10000, Rt_Ransac_Kneip, nameKneip, RtForMatlab_Ransac_Kneip);
            
//            for (int i=0; i<4; i++)
//            {
//                int ind = ransacKneip.model_[i];
//                std::cout << adapter.getBearingVector(ind) << " " << std::endl << std::endl;
//            };
//            
//            for (int j=0; j<4; j++) {
//                int ind2 = ransacKneip.model_[j];
//                std::cout << adapter.getPoint(ind2) << " " << std::endl << std::endl;
//            };
            
            usleep(1000000);
            
            
//            drawHypothesizeAndTestSets(ImgOld, Img1, ransacKneip, points3DRiprojVector, points2DLeftHomVector, 100, 2.);
//            
//            drawTrackedInliers(ImgOld, Img1, ransacKneip, points3DRiprojVector, points2DLeftHomVector, 50, 2.);
            
            
            ///////////////////////////////////////////// GAO ///////////////////////////////////////////////////
            
            // create a Ransac object
            opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> ransacGao;
            
            // create an AbsolutePoseSacProblem (algorithm: GAO)
            boost::shared_ptr<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem>
            absposeproblem_Gao(new opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem(adapter, opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem::GAO));
            
            std::string nameGao = "Current pose (Ransac - Gao algorithm): ";
            
            runRansac(ransacGao, adapter, absposeproblem_Gao, treshold, 10000, Rt_Ransac_Gao, nameGao, RtForMatlab_Ransac_Gao);
            
            
            
            ///////////////////////////////////////////// EPNP ///////////////////////////////////////////////////
            
            // create a Ransac object
            opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> ransacEpnp;
            
            // create an AbsolutePoseSacProblem (algorithm: EPNP)
            boost::shared_ptr<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem>
            absposeproblem_Epnp(new opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem(adapter, opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem::EPNP));
            
            std::string nameEpnp = "Current pose (Ransac - EPnP algorithm): ";
            
            runRansac(ransacEpnp, adapter, absposeproblem_Epnp, treshold, 20000, Rt_Ransac_Epnp, nameEpnp, RtForMatlab_Ransac_Epnp);
            
            ///////////////////////////// NON-LINEAR OPTIMIZATION POST RANSAC KNEIP ////////////////////////////////////
            
            std::string nameNonLinKneip = "Current pose (non-linear optimization - Kneip algorithm): ";
            
            nonLinearOptimization(ransacKneip, bearingVectorsForRansac, pointsForRansac, Rt_Ransac_Kneip, Rt_NonLin_Kneip, nameNonLinKneip, RtForMatlab_NonLinKneip);
            
            ///////////////////////////// NON-LINEAR OPTIMIZATION POST RANSAC GAO ////////////////////////////////////
            
            std::string nameNonLinGao = "Current pose (non-linear optimization - Gao algorithm): ";
            
            nonLinearOptimization(ransacGao, bearingVectorsForRansac, pointsForRansac, Rt_Ransac_Gao, Rt_NonLin_Gao, nameNonLinGao, RtForMatlab_NonLinGao);
            
            ///////////////////////////// NON-LINEAR OPTIMIZATION POST RANSAC EPNP ////////////////////////////////////
            
            std::string nameNonLinEpnp = "Current pose (non-linear optimization - EPnP algorithm): ";
            
            nonLinearOptimization(ransacEpnp, bearingVectorsForRansac, pointsForRansac, Rt_Ransac_Epnp, Rt_NonLin_Epnp, nameNonLinEpnp, RtForMatlab_NonLinEpnp);
            
            
            Rt = Rt_NonLin_Kneip;
            
        }
        
        // write bearing vectors, points and the transormation obtained to text file (Matlab debug)
        bearingVectorsWrite(bearingVectorsForMatlab);
        pointsWrite(pointsForMatlab);
        
        std::string RansacStringKneip = "RansacKneip";
        std::string RansacStringGao =   "RansacGao";
        std::string RansacStringEpnp =  "RansacEpnp";
        std::string NonLinKneipString = "NonLinKneip";
        std::string NonLinGaoString =   "NonLinGao";
        std::string NonLinEpnpString =   "NonLinEpnp";
        RtWriteTxt(RtForMatlab_Ransac_Kneip, RansacStringKneip);
        RtWriteTxt(RtForMatlab_Ransac_Gao, RansacStringGao);
        RtWriteTxt(RtForMatlab_Ransac_Epnp, RansacStringEpnp);
        RtWriteTxt(RtForMatlab_NonLinKneip, NonLinKneipString);
        RtWriteTxt(RtForMatlab_NonLinGao, NonLinGaoString);
        RtWriteTxt(RtForMatlab_NonLinEpnp, NonLinEpnpString);
        
        ////////////////////////////////////////////////////////////////////////////
        
        
        // sort matches (best score)
        std::sort(matches.begin(), matches.end(), compareMatch);
        
//        double matchesSizeInit = matches.size();
        
        // eliminate matches with score too low (distance > 0.1)
        int ind_match = 0;
        
        while (matches[ind_match].distance < outlierThreshold && ind_match < matches.size())
        {
            ind_match++;
        }
        matches.resize(ind_match);
        
//        std::cout << "Matches outliers: " << matches.size() << "/" << matchesSizeInit << std::endl << std::endl;
        
        
                // Draw best matches (debug)
        drawBestMatches(matches, Img1, Img2, keypointsL, keypointsR, 40, counter, 2.);
        cv::Mat ImgOut;
        drawKeypoints(Img1, keypointsL, ImgOut, cv::Scalar::all(-1), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS );
        cv::resize(ImgOut, ImgOut, cv::Size(ImgOut.cols/2,ImgOut.rows/2));
        cv::imwrite("/Users/giuliasolda/Desktop/VO_software/Images/keypoints.jpg", ImgOut);

        
            

        
        for (int m=0; m<matches.size(); m++)
        {
            // bearing vectors with left keypoints
            point2DLeftHomSort(0) = keypointsL[matches[m].queryIdx].pt.x;
            point2DLeftHomSort(1) = keypointsL[matches[m].queryIdx].pt.y;
            point2DLeftHomSort(2) = 1.;
            points2DLeftHomVectorOld.push_back(point2DLeftHomSort);
            bearingVectorLeftSorted = CM1Inv*point2DLeftHomSort;
            bearingVectorLeftSorted/= bearingVectorLeftSorted.norm();
            bearingVectorsLeftSorted.push_back(bearingVectorLeftSorted);
            
            // bearing vectors with right keypoints
            point2DRightHomSort(0) = keypointsR[matches[m].trainIdx].pt.x;
            point2DRightHomSort(1) = keypointsR[matches[m].trainIdx].pt.y;
            point2DRightHomSort(2) = 1.;
            bearingVectorRightSorted = CM2Inv*point2DRightHomSort;
            bearingVectorRightSorted/= bearingVectorRightSorted.norm();
            bearingVectorsRightSorted.push_back(bearingVectorRightSorted);
            
        };
        
        // create a central relative adapter
        opengv::relative_pose::CentralRelativeAdapter adapterTriangulation(bearingVectorsLeftSorted, bearingVectorsRightSorted, trasl12, rotMat12);
        
        int q = 0;
        
        for (int t=0; t<bearingVectorsLeftSorted.size(); t++)
        {
            // triangulate points
            opengv::point_t pointTriangulate = opengv::triangulation::triangulate(adapterTriangulation, t);
            
            
            // point 3D reprojection
            cv::Mat pointTriangulateHom = cv::Mat(4,1, CV_64F);
            for (int p=0; p<3; p++){
                pointTriangulateHom.at<double>(p,0) = pointTriangulate(p);
            }
            pointTriangulateHom.at<double>(3,0) = 1;
            
            cv::Mat pointTriangulateRiprojHom = projMat1*pointTriangulateHom;
            cv::Point2f pointTriangulateRiproj = cv::Point2f(pointTriangulateRiprojHom.at<double>(0,0)/pointTriangulateRiprojHom.at<double>(2,0), pointTriangulateRiprojHom.at<double>(1,0)/pointTriangulateRiprojHom.at<double>(2,0));
            
            
            // corrispondent point 2D
            cv::Point2f point2D = cv::Point2f(points2DLeftHomVectorOld[t](0)/points2DLeftHomVectorOld[t](2), points2DLeftHomVectorOld[t](1)/points2DLeftHomVectorOld[t](2));
            
            
            //            std::cout << points2DLeftHomVectorOld[t] << " " << point2D << std::endl;
            
            double distance2D3D = cv::norm(pointTriangulateRiproj-point2D);
            
            
            if (distance2D3D >= 2)
            {
                matches.erase(matches.begin()+q);
            }
            else
            {
                points3DTriangulate.push_back(pointTriangulate);
//                pointsDebug.push_back(pointTriangulate);
//                point2DVector.push_back(points2DLeftHomVectorOld[t]);
                q++;
            }
            
            
        

            
            
            
            
            
            
        }
        
        
        
//        points2D3Dcorrispondences(Img1, projMat1, point2DVector, pointsDebug, 20, counter, 1.5);
        
        
        
        // prepare the elements for the next iteration
        //        bearingVectorsLeftOld = bearingVectorsLeftSorted;
        //bearingVectorsRightOld = bearingVectorsRightSorted;
        points3DOld = points3DTriangulate;
        matchesOld = matches;
        keypointsOld = keypointsL;
        descriptorOld = descriptorL;
        
        points3DTriangulate.clear();
        bearingVectorsLeftSorted.clear();
        bearingVectorsRightSorted.clear();
        bearingVectorsLeft.clear();
        bearingVectorsRight.clear();
        points.clear();
        points2DLeftHomVector.clear();
        points2DRightHomVector.clear();
        points2DLeftHomVectorOld.clear();
        points3DRiprojVector.clear();
        
        //        bearingVectorsExitRansac.clear();
        //        pointsExitRansac.clear();
        //        bearingVectorsExitRansacGao.clear();
        //        pointsExitRansacGao.clear();
        
        points2DLeft.clear();
        points2DRight.clear();
        
        // debug
        Img1.copyTo(ImgOld);
        pointsWithoutOutliers.clear();
        points2DLeftHomVectorWithoutOutliers.clear();
        bearingVectorsLeftWithoutOutliers.clear();
        
        
        
        
//        point2DVector.clear();
//        pointsDebug.clear();
    };
    
    
private:
    
    int counter = 0;
    
    cv::Mat Img1, Img2, Img1Und, Img2Und;
    cv::Mat CM1, CM2, K1, K2, R12, T12, imgSize;
    cv::Mat RtCam1, RtCam2;
    cv::Mat projMat1, projMat2;
    Eigen::Matrix3d CM1Inv, CM2Inv;
    
    // detector
    int feature;
    cv::StarDetector featuresStar;
    cv::SIFT featuresSift;
    cv::SURF featureSurf;
    cv::FastFeatureDetector featureFast;
    cv::SurfFeatureDetector detectorSurf;
    cv::ORB featureOrb;
    std::vector<cv::KeyPoint> keypointsL;
    std::vector<cv::KeyPoint> keypointsR;
    std::vector<cv::KeyPoint> keypointsOld;
    
    // descriptor
    cv::Mat descriptorL, descriptorR;
    cv::Mat descriptorOld;
    cv::SiftDescriptorExtractor descriptorSift;
    cv::BriefDescriptorExtractor descriptorBrief;
//    cv::SurfDescriptorExtractor descriptorSurf;
    cv::SurfDescriptorExtractor extractorSurf;
    
    // matching
    cv::BFMatcher matcher;
    std::vector <cv::DMatch> matches;
    std::vector <cv::DMatch> matchesDt;
    std::vector <cv::DMatch> matchesOld;
    
    
    // matches sorting
    static bool compareMatch(const cv::DMatch &a, const cv::DMatch &b)
    {
        return a.distance<b.distance;
    }
    
    // 2d points
    Eigen::Vector3d point2DLeftHom;
    Eigen::Vector3d point2DRightHom;
    Eigen::Vector3d point2DLeftHomSort;
    Eigen::Vector3d point2DRightHomSort;
    Eigen::Vector3d point3DRiprojVec;
    
    // opencv 2d points
    cv::Point2f point2DLeft;
    cv::Point2f point2DRight;
    
    // opengv bearing vectors
    opengv::bearingVector_t bearingVectorLeft;
    opengv::bearingVector_t bearingVectorRight;
    opengv::bearingVector_t bearingVectorLeftSorted;
    opengv::bearingVector_t bearingVectorRightSorted;
    opengv::bearingVectors_t bearingVectorsLeft;
    opengv::bearingVectors_t bearingVectorsRight;
    //    opengv::bearingVectors_t bearingVectorsLeftOld;
    opengv::bearingVectors_t bearingVectorsRightOld;
    opengv::bearingVectors_t bearingVectorsLeftSorted;
    opengv::bearingVectors_t bearingVectorsRightSorted;
    opengv::points_t points;
    
    //    opengv::bearingVectors_t bearingVectorsExitRansac;
    //    opengv::points_t pointsExitRansac;
    opengv::bearingVectors_t bearingVectorsExitRansacGao;
    opengv::points_t pointsExitRansacGao;
    
    
    opengv::translation_t initialTranslation;
    opengv::rotation_t initialRotation;
    
    // opengv triangulation
    opengv::translation_t trasl12;
    opengv::rotation_t rotMat12;
    opengv::point_t pointTriangulate;
    std::vector<opengv::point_t> points3DTriangulate;
    std::vector<opengv::point_t> points3DOld;
    
    
    // debug
    std::vector <Eigen::Vector3d> points2DLeftHomVector;
    std::vector <Eigen::Vector3d> points2DLeftHomVectorWithoutOutliers;
    std::vector <Eigen::Vector3d> points2DRightHomVector;
    std::vector <Eigen::Vector3d> points2DLeftHomVectorOld;
    std::vector <Eigen::Vector3d> points3DRiprojVector;
    std::vector <cv::Point2f> points2DLeft;
    std::vector <cv::Point2f> points2DRight;
    std::vector <opengv::bearingVectors_t > bearingVectorsForMatlab;
    std::vector <opengv::points_t > pointsForMatlab;
    opengv::transformations_t RtForMatlab_Ransac_Kneip;
    opengv::transformations_t RtForMatlab_Ransac_Gao;
    opengv::transformations_t RtForMatlab_Ransac_Epnp;
    opengv::transformations_t RtForMatlab_PnP;
    opengv::transformations_t RtForMatlab_NonLinKneip;
    opengv::transformations_t RtForMatlab_NonLinGao;
    opengv::transformations_t RtForMatlab_NonLinEpnp;
    
    // debug
    cv::Mat ImgOld;
    //double distance2D3Driproj;
    opengv::bearingVectors_t bearingVectorsLeftWithoutOutliers;
    opengv::points_t pointsWithoutOutliers;
    cv::Mat point3DHom;
    cv::Mat point3DRiprojHom;
    cv::Point2f point3DRiproj;
    
    
    opengv::transformation_t Rt_Ransac_Kneip;
    opengv::transformation_t Rt_Ransac_Gao;
    opengv::transformation_t Rt_Ransac_Epnp;
    opengv::transformation_t Rt_NonLin_Kneip;
    opengv::transformation_t Rt_NonLin_Gao;
    opengv::transformation_t Rt_NonLin_Epnp;
    
    double outlierThreshold;
    
    // timing
    std::vector <double> timeDetection;
    std::vector <double> timeDescription;
    
    
    
//    std::vector<Eigen::Vector3d> point2DVector;
//    opengv::points_t pointsDebug;
    
};


// Define a rotation matrix given rotation axis and magnitude
void rotationMatrix(cv::Mat &R, int axis, double angle)
{
    switch (axis)
    {
        case 1:
            R.at<double>(0,0) = 1;
            R.at<double>(1,1) = cos(angle);
            R.at<double>(1,2) = -sin(angle);
            R.at<double>(2,1) = sin(angle);
            R.at<double>(2,2) = cos(angle);
            break;
        case 2:
            R.at<double>(0,0) = cos(angle);
            R.at<double>(0,2) = sin(angle);
            R.at<double>(1,1) = 1;
            R.at<double>(2,0) = -sin(angle);
            R.at<double>(2,2) = cos(angle);
            break;
        case 3:
            R.at<double>(0,0) = cos(angle);
            R.at<double>(0,1) = -sin(angle);
            R.at<double>(1,0) = sin(angle);
            R.at<double>(1,1) = cos(angle);
            R.at<double>(2,2) = 1;
            break;
    }
};

// Calculate rotation matrix given three angle and three axis
void eulerToRotation(cv::Mat &R, double alpha, double beta, double gamma, int first, int second, int third)
{
    cv::Mat A(3,3,CV_64F,0.f);
    cv::Mat B(3,3,CV_64F,0.f);
    cv::Mat C(3,3,CV_64F,0.f);
    rotationMatrix(A, first, gamma);
    rotationMatrix(B, second, beta);
    rotationMatrix(C, third, alpha);
    R = C*B*A;
};


#endif // VISUAL_ODOMETRY_H