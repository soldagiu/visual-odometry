#ifndef DEBUG_METHODS_H
#define DEBUG_METHODS_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/nonfree/nonfree.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <opengv/absolute_pose/methods.hpp>
#include <opengv/absolute_pose/CentralAbsoluteAdapter.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <opengv/triangulation/methods.hpp>
#include <opengv/sac/Ransac.hpp>
#include <opengv/sac_problems/absolute_pose/AbsolutePoseSacProblem.hpp>

// draw 2D left points at time t and 3D reprojected point at time t-dt on image (at time t)
// save the figure in "Images"
void points2D3Dcorrispondences(cv::Mat & Img, cv::Mat & projMat, std::vector<Eigen::Vector3d> & point2DVector, opengv::points_t & points, int numberCorrespondences, int counter, double resize);

void points2D3DcorrispondencesLeftRight(cv::Mat & Img1, cv::Mat & projMat1, std::vector<Eigen::Vector3d> & points2DLeft, cv::Mat & Img2, cv::Mat & projMat2, std::vector<Eigen::Vector3d> & points2DRight, opengv::points_t & points, int numberCorrespondences);

// store the left bearing vectors (those used in Ransac)
void bearingVectorsStore(opengv::bearingVectors_t & bearingVectors, std::vector <opengv::bearingVectors_t > &bearingVectorsForMatlab);

// write the bearing vectors user in Ransac to text file (for Matlab debug)
void bearingVectorsWrite(std::vector <opengv::bearingVectors_t > & bearingVectorsForMatlab);

// store the 3D points (those used in Ransac)
void pointsStore(opengv::points_t & points, std::vector <opengv::points_t > &pointsForMatlab);

// write the 3D points used in Ransac to text file (for Matlab debug)
void pointsWrite(std::vector <opengv::points_t > pointsForMatlab);

// store the Rt transformations (Ransac output) and write to text file (for Matlab debug)
void RtWriteTxt(opengv::transformations_t & RtForMatlab, std::string opt);

// store the 2D points and write a text file (for Matlab debug)
void points2DStore(std::vector <cv::Point2f> points, std::vector < std::vector <cv::Point2f> > &points2DForMatlab, int numberPoint);
void points2DWrite(std::vector < std::vector <cv::Point2f> > points2DForMatlab, std::string name);


// display a resized figure
void showFigure(const cv::Mat &imIn, const int &w, const int &h);

// Draw best left-right matches
void drawBestMatches(std::vector <cv::DMatch> & matches, cv::Mat & Img1, cv::Mat & Img2, std::vector<cv::KeyPoint> & keypointsL, std::vector<cv::KeyPoint> & keypointsR, int numPoints, int counter, double resize);

// Draw tracked matches
void drawTrackedMatches(std::vector <cv::DMatch> & matches, std::vector<cv::KeyPoint> & keypointsOld, std::vector<cv::KeyPoint> & keypoints, cv::Mat & ImgOld, cv::Mat & Img, int &counter, int numPoints, double resize);

// Print difference between non-distorted and distorted image
void printDifference(cv::Mat CM1, cv::Mat K1, bool correct, double resize);

// draw ransac hypothesize and consensus set
void drawHypothesizeAndTestSets(cv::Mat &ImgOld, cv::Mat &Img, opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, std::vector <Eigen::Vector3d> &points3DRiproj, std::vector <Eigen::Vector3d> &points2D,  int numPoints, double resize);

// Draw tracked matches highlighting inliers and outliers
void drawTrackedInliers(cv::Mat &ImgOld, cv::Mat &Img, opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, std::vector <Eigen::Vector3d> &points3DRiproj, std::vector <Eigen::Vector3d> &points2D,  int numPoints, double resize);

// select a visible 2d point in both figure, triangulate and then reproject the point
void riproiettoPunti2DNoti();


#endif // DEBUG_METHODS_H
