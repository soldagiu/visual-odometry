#include "debug_methods.h"


void points2D3Dcorrispondences(cv::Mat & Img, cv::Mat & projMat, std::vector<Eigen::Vector3d> & points2DVector, opengv::points_t & points, int numberCorrespondences, int counter, double resize)
{
    std::stringstream ss;
    for (int i=0; i<numberCorrespondences; i++)
    {
        ss.str("");
        ss << i;
        cv::putText(Img, ss.str(), cv::Point2f(points2DVector[i][0], points2DVector[i][1]), cv::FONT_HERSHEY_SCRIPT_SIMPLEX, 1.5, cv::Scalar(0,0,255),2);
        
        cv::Mat punto(4,1, CV_64F);
        for (int p=0; p<3; p++)
        {
            punto.at<double>(p,0) = points[i](p);
        }
        punto.at<double>(3,0) = 1;
        
        cv::Mat punti3DRipr = projMat*punto;
        cv::Point2f punto3D2D(punti3DRipr.at<double>(0,0)/punti3DRipr.at<double>(2,0), punti3DRipr.at<double>(1,0)/punti3DRipr.at<double>(2,0));
        cv::putText(Img, ss.str(), punto3D2D, cv::FONT_HERSHEY_SCRIPT_SIMPLEX, 2, cv::Scalar(0,255,0),2);
    }
    
    ss.str("");
    ss << counter;
    cv::resize(Img, Img, cv::Size(Img.cols/resize,Img.rows/resize));
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/images/2D_3D_corrispondences/" + ss.str() + ".jpg", Img);
    
}

void points2D3DcorrispondencesLeftRight(cv::Mat & Img1, cv::Mat & projMat1, std::vector<Eigen::Vector3d> & points2DLeft, cv::Mat & Img2, cv::Mat & projMat2, std::vector<Eigen::Vector3d> & points2DRight, opengv::points_t & points, int numberCorrespondences)
{
    points2D3Dcorrispondences(Img1, projMat1, points2DLeft, points, numberCorrespondences, 0, 1);
    points2D3Dcorrispondences(Img2, projMat2, points2DRight, points, numberCorrespondences, 0, 1);
    cv::Mat Im_corr;
    hconcat(Img1, Img2, Im_corr);
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/Images/2D3Dcorrispondences.png", Im_corr);
}

void bearingVectorsStore(opengv::bearingVectors_t & bearingVectors, std::vector <opengv::bearingVectors_t > &bearingVectorsForMatlab)
{
    opengv::bearingVectors_t vettoreBearing(bearingVectors.begin(),bearingVectors.begin()+10);
    bearingVectorsForMatlab.push_back(vettoreBearing);
}

void bearingVectorsWrite(std::vector <opengv::bearingVectors_t > & bearingVectorsForMatlab)
{
    std::ofstream myfile_write;
    myfile_write.open ("/Users/giuliasolda/Desktop/VO_software/Matlab_functions/txt_results/bearingVectors.txt");
    myfile_write << "[";
    for(int i=0; i<bearingVectorsForMatlab.size(); i++)
    {
        opengv::bearingVectors_t stampa = bearingVectorsForMatlab[i];
        myfile_write << "{";
        for(int j=0; j<stampa.size(); j++)
        {
            myfile_write << "[" << stampa[j](0) << "," << stampa[j](1) << "," << stampa[j](2) << "]";
            if (j<stampa.size()-1)
                myfile_write << ",";
        }
        myfile_write << "}";
        if (i<bearingVectorsForMatlab.size()-1)
            myfile_write << ",";
    }
    myfile_write << "]";
    myfile_write.close();
}


void pointsStore(opengv::points_t & points, std::vector <opengv::points_t > &pointsForMatlab)
{
    opengv::points_t vettorePoint(points.begin(),points.begin()+10);
    pointsForMatlab.push_back(vettorePoint);
}

void pointsWrite(std::vector <opengv::points_t > pointsForMatlab)
{
    std::ofstream myfile_write_1;
    myfile_write_1.open ("/Users/giuliasolda/Desktop/VO_software/Matlab_functions/txt_results/points.txt");
    myfile_write_1 << "[";
    for(int i=0; i<pointsForMatlab.size(); i++)
    {
        opengv::points_t stampa = pointsForMatlab[i];
        myfile_write_1 << "{";
        for(int j=0; j<stampa.size(); j++)
        {
            myfile_write_1 << "[" << stampa[j](0) << "," << stampa[j](1) << "," << stampa[j](2) << "]";
            if (j<stampa.size()-1)
                myfile_write_1 << ",";
        }
        myfile_write_1 << "}";
        if (i<pointsForMatlab.size()-1)
            myfile_write_1 << ",";
    }
    myfile_write_1 << "]";
    myfile_write_1.close();
}


void RtWriteTxt(opengv::transformations_t & RtForMatlab, std::string opt)
{
    std::stringstream ss;
    ss.str("");
    ss << opt;
    std::ofstream myfile_write_2;
    myfile_write_2.open ("/Users/giuliasolda/Desktop/VO_software/Matlab_functions/txt_results/Rt" + ss.str() +".txt");
    myfile_write_2 << "[";
    for(int i=0; i<RtForMatlab.size(); i++)
    {
        opengv::transformation_t stampa = RtForMatlab[i];
        myfile_write_2 << "{[";
        for (int r=0; r<3; r++)
        {
            myfile_write_2 << "[" <<stampa(r,0) << "," << stampa(r,1) << "," << stampa(r,2) << "," << stampa(r,3) << "]";
            if (r<2)
                myfile_write_2 << ",";
        }
        myfile_write_2 << "]}";
        if (i<RtForMatlab.size()-1)
            myfile_write_2 << ",";
    }
    myfile_write_2 << "]";
    myfile_write_2.close();
}

void points2DStore(std::vector <cv::Point2f> points, std::vector < std::vector <cv::Point2f> > &points2DForMatlab, int numberPoint)
{
    std::vector <cv::Point2f> vettorePoint(points.begin(),points.begin()+numberPoint);
    points2DForMatlab.push_back(vettorePoint);
}

void points2DWrite(std::vector < std::vector <cv::Point2f> > points2DForMatlab, std::string name)
//, std::vector <cv::Point2f> points2, std::vector<opengv::point_t> points3DTriangulate)
{
    std::ofstream myfile_write_1;
    myfile_write_1.open ("/Users/giuliasolda/Desktop/VO_software/Matlab_functions/txt_results/points2D" + name + ".txt");
    myfile_write_1 << "[";
    for(int i=0; i<points2DForMatlab.size(); i++)
    {
        std::vector <cv::Point2f> stampa = points2DForMatlab[i];
        myfile_write_1 << "{";
        for(int j=0; j<stampa.size(); j++)
        {
            myfile_write_1 << "[" << stampa[j].x << "," << stampa[j].y << "]";
            if (j<stampa.size()-1)
                myfile_write_1 << ",";
        }
        myfile_write_1 << "}";
        if (i<points2DForMatlab.size()-1)
            myfile_write_1 << ",";
    }
    myfile_write_1 << "]";
    myfile_write_1.close();
}


void showFigure(const cv::Mat &imIn, const int &w, const int &h)
{
    cv::Mat imOut;
    resize(imIn, imOut, cv::Size(w,h));
    imshow("Img", imOut);
    cv::waitKey(0);
}


void drawBestMatches(std::vector <cv::DMatch> & matches, cv::Mat & Img1, cv::Mat & Img2, std::vector<cv::KeyPoint> & keypointsL, std::vector<cv::KeyPoint> & keypointsR, int numPoints, int counter, double resize)
{
    std::stringstream ss;
    ss.str("");
    ss << counter;
    // keypoints on the camera planes
    cv::Mat points2DL;
    cv::Mat points2DR;
    // create a vector of best matches
    int numMatches = 10;
    std::vector <cv::DMatch> bestMatches(numMatches);
    std::copy(matches.begin(), matches.begin()+numMatches, bestMatches.begin());
    // Draw numMatches best matches
    cv::Mat Img_match;
    cv::drawMatches(Img1, keypointsL, Img2, keypointsR, bestMatches, Img_match, cv::Scalar::all(-1), cv::Scalar::all(-1), std::vector<char>() , 4);
    //showFigure(Img_match, 2560, 682);
    cv::resize(Img_match, Img_match, cv::Size(Img_match.cols/resize,Img_match.rows/resize));
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/Images/matches/" + ss.str() + ".jpg", Img_match);
    
    // sort keypoints on both the camera planes (with respect to matches)
    points2DL = cv::Mat(2, matches.size(), CV_64F);
    points2DR = cv::Mat(2, matches.size(), CV_64F);
    for (int i=0; i<matches.size(); i++)
    {
        points2DL.at<float>(0,i) = keypointsL[matches[i].queryIdx].pt.x;
        points2DL.at<float>(1,i) = keypointsL[matches[i].queryIdx].pt.y;
        points2DR.at<float>(0,i) = keypointsR[matches[i].trainIdx].pt.x;
        points2DR.at<float>(1,i) = keypointsR[matches[i].trainIdx].pt.y;
    }
    // Draw circle around the best features
    // create a 1x1 matrix
    cv::Mat colour(1,1, CV_8UC3, cvScalar(0,0,0));
    for (int ind=0; ind<numPoints; ind++)
    {
        // convert the matrix to hsv
        cvtColor(colour, colour, CV_BGR2HSV);
        // initialize an hsv colour
        colour.at<cv::Vec3b>(0,0)=cv::Vec3b(ind*255/numPoints,255,255);
        // convert to rgb
        cvtColor(colour, colour, CV_HSV2BGR);
        // the rgb colour is then...
        cv::Vec3b colourInRGB = colour.at<cv::Vec3b>(0,0);
        uchar blue = colourInRGB[0];
        uchar green = colourInRGB[1];
        uchar red = colourInRGB[2];
        cv::circle(Img1, cv::Point2i(points2DL.at<float>(0,ind),points2DL.at<float>(1,ind)), keypointsL[matches[ind].queryIdx].size, cv::Scalar_<float>(blue, green, red), 3);
        cv::circle(Img2, cv::Point2i(points2DR.at<float>(0,ind),points2DR.at<float>(1,ind)), keypointsR[matches[ind].trainIdx].size, cv::Scalar_<float>(blue, green, red), 3);
        
    };
    
    
    // Draw best numPoints matches
    cv::Mat Im_check;
    hconcat(Img1, Img2, Im_check);
    //showFigure(Im_check, 1280, 341);
    cv::resize(Im_check, Im_check, cv::Size(Im_check.cols/resize,Im_check.rows/resize));
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/Images/bestMatches/" + ss.str() + ".jpg", Im_check);
}



void drawTrackedMatches(std::vector <cv::DMatch> & matches, std::vector<cv::KeyPoint> & keypointsOld, std::vector<cv::KeyPoint> & keypoints, cv::Mat & ImgOld, cv::Mat & Img, int &counter, int numPoints, double resize)
{
    cv::Mat colour(1,1, CV_8UC3, cvScalar(0,0,0));
    cv::Mat ImgOldCopia(ImgOld.size(), ImgOld.type());
    ImgOld.copyTo(ImgOldCopia);
    cv::Mat ImgCopia(Img.size(), Img.type());
    Img.copyTo(ImgCopia);                   // c'era un errore! controllare!
    for (int ind=0; ind<numPoints; ind++)
    {
        // convert the matrix to hsv
        cvtColor(colour, colour, CV_BGR2HSV);
        // initialize an hsv colour
        colour.at<cv::Vec3b>(0,0)=cv::Vec3b(ind*255/numPoints,255,255);
        // convert to rgb
        cvtColor(colour, colour, CV_HSV2BGR);
        // the rgb colour is then...
        cv::Vec3b colourInRGB = colour.at<cv::Vec3b>(0,0);
        uchar blue = colourInRGB[0];
        uchar green = colourInRGB[1];
        uchar red = colourInRGB[2];
        cv::circle(ImgOldCopia, cv::Point2f(keypointsOld[matches[ind].queryIdx].pt.x, keypointsOld[matches[ind].queryIdx].pt.y),  keypointsOld[matches[ind].queryIdx].size, cv::Scalar_<float>(blue, green, red), 3);
        cv::circle(ImgCopia, cv::Point2f(keypoints[matches[ind].trainIdx].pt.x, keypoints[matches[ind].trainIdx].pt.y),  keypoints[matches[ind].trainIdx].size, cv::Scalar_<float>(blue, green, red), 3);

    };
    std::stringstream ss;
    ss.str("");
    ss << counter;
    cv::Mat Im_track;
    
    vconcat(ImgOldCopia, ImgCopia, Im_track);

    cv::resize(Im_track, Im_track, cv::Size(Im_track.cols/resize,Im_track.rows/resize));
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/Images/tracking/" + ss.str() + ".jpg", Im_track);

}

void printDifference(cv::Mat CM1, cv::Mat K1, bool correct, double resize)
{
    cv::Mat ImOrig = cv::imread("/Users/giuliasolda/Desktop/dati/posizione1/traslazione/originali/Left/MilSequence2_traslazione_rip00000.tif",0);
    cv::Mat ImOrig3, ImOrigUnd;
    // demosaicing
    cvtColor(ImOrig, ImOrig3, CV_BayerGR2BGR);
    // flip images
    flip(ImOrig3, ImOrig3, -1);
    if (correct){
        // undistort
        cv::undistort(ImOrig3, ImOrigUnd, CM1, K1);
    }
    else
        ImOrigUnd = ImOrig3;
    
    cv::Mat ImProf = cv::imread("/Users/giuliasolda/Desktop/dati/posizione1/traslazione/Left/MilSequence2_traslazione_rip00_corr_001.tif");
    
    // get the difference
    cv::Mat difference;
    difference = ImOrigUnd - ImProf;
    cv::Mat difference_bis(difference.rows,difference.cols, CV_32F);
    for (int c = 0; c<difference.cols; c++)
    {
        for (int r = 0; r<difference.rows; r++)
        {
            uchar blu = difference.at <cv::Vec3b> (r,c)[0];
            uchar verde = difference.at <cv::Vec3b> (r,c)[1];
            uchar rosso = difference.at <cv::Vec3b> (r,c)[2];
            float modulo = sqrt(blu*blu+verde*verde+rosso*rosso);
            difference_bis.at<float> (r,c) = modulo/sqrt(195075);
        }
    }
    
    difference_bis.convertTo(difference_bis, CV_8UC1, 255);
    cv::resize(difference_bis, difference_bis, cv::Size(difference_bis.cols/resize,difference_bis.rows/resize));
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/images/undistort.jpeg", difference_bis);
}

void drawHypothesizeAndTestSets(cv::Mat &ImgOld, cv::Mat &Img, opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, std::vector <Eigen::Vector3d> &points3DRiproj, std::vector <Eigen::Vector3d> &points2D,  int numPoints, double resize)
{
    cv::Mat colour(1,1, CV_8UC3, cvScalar(0,0,0));
    cv::Mat ImgOldCopia(ImgOld.size(), ImgOld.type());
    ImgOld.copyTo(ImgOldCopia);
    cv::Mat ImgCopia(Img.size(), Img.type());
    Img.copyTo(ImgCopia);
    //            int numPoints = ransacKneip.inliers_.size();
    for (int i=0; i<numPoints; i++)
    {
        //                int ind = ransacKneip.model_[i];
        int ind = ransac.inliers_[i];
        // convert the matrix to hsv
        cvtColor(colour, colour, CV_BGR2HSV);
        // initialize an hsv colour
        colour.at<cv::Vec3b>(0,0)=cv::Vec3b(ind*255/numPoints,255,255);
        // convert to rgb
        cvtColor(colour, colour, CV_HSV2BGR);
        // the rgb colour is then...
        cv::Vec3b colourInRGB = colour.at<cv::Vec3b>(0,0);
        uchar blue = colourInRGB[0];
        uchar green = colourInRGB[1];
        uchar red = colourInRGB[2];
        cv::circle(ImgOldCopia, cv::Point2i(points3DRiproj[ind](0), points3DRiproj[ind](1)), 20, cv::Scalar_<float>(blue, green, red), 3);
        cv::circle(ImgCopia, cv::Point2i(points2D[ind](0), points2D[ind](1)), 20, cv::Scalar_<float>(blue, green, red), 3);
    };
    cv::Mat Im_track1;
    hconcat(ImgOldCopia, ImgCopia, Im_track1);
    cv::resize(Im_track1, Im_track1, cv::Size(Im_track1.cols/resize,Im_track1.rows/resize));
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/images/Ransac/ransac_inliers.jpeg", Im_track1);
    
    cv::Mat ImgOldCopia1(ImgOld.size(), ImgOld.type());
    ImgOld.copyTo(ImgOldCopia1);
    cv::Mat ImgCopia1(Img.size(), Img.type());
    Img.copyTo(ImgCopia1);
    int numPoints1 = ransac.model_.size();
    
    for (int i=0; i<numPoints1; i++)
    {
        int ind = ransac.model_[i];
        // convert the matrix to hsv
        cvtColor(colour, colour, CV_BGR2HSV);
        // initialize an hsv colour
        colour.at<cv::Vec3b>(0,0)=cv::Vec3b(ind*255/numPoints1,255,255);
        // convert to rgb
        cvtColor(colour, colour, CV_HSV2BGR);
        // the rgb colour is then...
        cv::Vec3b colourInRGB = colour.at<cv::Vec3b>(0,0);
        uchar blue = colourInRGB[0];
        uchar green = colourInRGB[1];
        uchar red = colourInRGB[2];
        cv::circle(ImgOldCopia1, cv::Point2i(points3DRiproj[ind](0), points3DRiproj[ind](1)), 20, cv::Scalar_<float>(blue, green, red), 3);
        cv::circle(ImgCopia1, cv::Point2i(points2D[ind](0), points2D[ind](1)), 20, cv::Scalar_<float>(blue, green, red), 3);
    };
    cv::Mat Im_track2;
    hconcat(ImgOldCopia1, ImgCopia1, Im_track2);
    cv::resize(Im_track2, Im_track2, cv::Size(Im_track2.cols/resize,Im_track2.rows/resize));
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/images/Ransac/ransac_hypotesis.jpeg", Im_track2);
}

void drawTrackedInliers(cv::Mat &ImgOld, cv::Mat &Img, opengv::sac::Ransac<opengv::sac_problems::absolute_pose::AbsolutePoseSacProblem> &ransac, std::vector <Eigen::Vector3d> &points3DRiproj, std::vector <Eigen::Vector3d> &points2D,  int numPoints, double resize)
{
    cv::Mat ImgOldCopia(ImgOld.size(), ImgOld.type());
    ImgOld.copyTo(ImgOldCopia);
    cv::Mat ImgCopia(Img.size(), Img.type());
    Img.copyTo(ImgCopia);
    
    for (int i=0; i<numPoints; i++)
    {
        if(std::find(ransac.inliers_.begin(), ransac.inliers_.end(), i) != ransac.inliers_.end()) {
            cv::Point2i puntosx(cv::Point2i(points3DRiproj[i](0), points3DRiproj[i](1)));
            cv::Point2i puntodx(cv::Point2i(points2D[i](0), points2D[i](1)));
            cv::circle(ImgOldCopia, puntosx, 20, cv::Scalar(0,255,0), 2);
            cv::circle(ImgCopia, puntodx, 20, cv::Scalar(0,255,0), 2);
        } else {
            cv::Point2i puntosx(cv::Point2i(points3DRiproj[i](0), points3DRiproj[i](1)));
            cv::Point2i puntodx(cv::Point2i(points2D[i](0), points2D[i](1)));
            cv::circle(ImgOldCopia, puntosx, 20, cv::Scalar(0,0,255), 2);
            cv::circle(ImgCopia, puntodx, 20, cv::Scalar(0,0,255), 2);
        }
    }
    
    cv::Mat Im_track2;
    vconcat(ImgOldCopia, ImgCopia, Im_track2);
    
    for (int j=0; j<numPoints; j++){
        if(std::find(ransac.inliers_.begin(), ransac.inliers_.end(), j) != ransac.inliers_.end()) {
            cv::Point2i pointMod(points2D[j](0), points2D[j](1)+ImgOldCopia.rows);
            cv::Point2i pointLeft(cv::Point2i(points3DRiproj[j](0), points3DRiproj[j](1)));
            cv::line(Im_track2, pointLeft, pointMod, cv::Scalar(0,255,0));
        } else {
            cv::Point2i pointMod(points2D[j](0), points2D[j](1)+ImgOldCopia.rows);
            cv::Point2i pointLeft(cv::Point2i(points3DRiproj[j](0), points3DRiproj[j](1)));
            cv::line(Im_track2, pointLeft, pointMod, cv::Scalar(0,0,255));
        }
    }
    cv::resize(Im_track2, Im_track2, cv::Size(ImgOldCopia.cols/resize,ImgOldCopia.rows*2/resize));
    cv::imwrite("/Users/giuliasolda/Desktop/VO_software/images/tracking_inliers_outliers.jpg", Im_track2);
}

//void riproiettoPunti2DNoti()
/*
{
    cv::Mat CM1, CM2, K1, K2, R12, T12;
    cv::FileStorage f;
    f.open("/Users/giuliasolda/Desktop/VO_Solda/parameters/camera_parameters.xml", cv::FileStorage::READ);
    f["Camera_Matrix_1"] >> CM1;            // camera matrix 1
    f["Camera_Matrix_2"] >> CM2;            // camera matrix 2
    f["Camera_Distortion_1"] >> K1;         // distortion coefficient camera 1
    f["Camera_Distortion_2"] >> K2;         // distortion coefficient camera 2
    f["R"] >> R12;            // rotation matrix from camera 1 to camera 2
    f["T"] >> T12;            // translation vector from camera 1 to camera 2
    f.release();
    
    std::string leftDirPath, rightDirPath, camParametersPath, outputImgPath;
    f.open("/Users/giuliasolda/Desktop/VO_Solda/parameters/paths.xml", cv::FileStorage::READ);
    f["leftDir"] >> leftDirPath;                // left images directory
    f["rightDir"] >> rightDirPath;              // right images directory
    f["camParameters"] >> camParametersPath;    // camera parameters directory
    f["outputImgDir"] >> outputImgPath;         // output images (debug) directory
    f.release();
    
    Eigen::Matrix3d CM1Inv;
    Eigen::Matrix3d CM2Inv;
    // Camera Matrix 1 inverse
    CM1Inv = Eigen::Matrix3d(CM1.ptr<double>(0));
    CM1Inv = CM1Inv.transpose().inverse();
    // Camera Matrix 2 inverse
    CM2Inv = Eigen::Matrix3d(CM2.ptr<double>(0));
    CM2Inv = CM2Inv.transpose().inverse();
    opengv::translation_t trasl12;
    opengv::rotation_t rotMat12;
    // prepare rotation and translation for triangulation
    rotMat12 = opengv::rotation_t(R12.ptr<double>(0)).transpose();
    trasl12 = opengv::translation_t(T12.ptr<double>(0)).transpose();
    cv::Mat RtCam1, RtCam2;
    RtCam1 = cv::Mat::eye(3, 4, CV_64F);
    hconcat(R12, T12, RtCam2);
    cv::Mat lastRow = cv::Mat::zeros(1,4, CV_64F);
    lastRow.at<double>(0,3) = 1;
    cv::Mat RtCam1_4x4, RtCam2_4x4;
    cv::Mat RtCam1Inv_4x4, RtCam2Inv_4x4;
    cv::Mat RtCam1Inv(3,4,CV_64F);
    cv::Mat RtCam2Inv(3,4,CV_64F);
    vconcat(RtCam1, lastRow, RtCam1_4x4);
    vconcat(RtCam2, lastRow, RtCam2_4x4);
    RtCam1Inv_4x4 = RtCam1_4x4.inv();
    RtCam2Inv_4x4 = RtCam2_4x4.inv();
    for (int r=0; r<3; r++) {
        for (int c=0; c<4; c++) {
            RtCam1Inv.at<double>(r,c) = RtCam1Inv_4x4.at<double>(r,c);
            RtCam2Inv.at<double>(r,c) = RtCam2Inv_4x4.at<double>(r,c);
        }
    };
    cv::Mat projMat1, projMat2;
    projMat1 = CM1*RtCam1Inv;
    projMat2 = CM2*RtCam2Inv;
    
    cv::Mat ImL = cv::imread(leftDirPath + "MilSequence2_traslazione_rip00000.tif", 0);
    cv::Mat ImR = cv::imread(rightDirPath + "MilSequence1_traslazione_rip00000.tif", 0);
    cv::Mat ImL3, ImR3;
    cvtColor(ImL, ImL3, CV_BayerGR2BGR);
    cvtColor(ImR, ImR3, CV_BayerGR2BGR);
    // flip images
    flip(ImL3, ImL3, -1);
    flip(ImR3, ImR3, -1);
    
    cv::Point2d point2DLeft, point2DRight;
    point2DLeft.x = 853;
    point2DLeft.y = 646;
    point2DRight.x = 650;
    point2DRight.y = 669;
    
    opengv::bearingVector_t point2DLeftHom, point2DRightHom;
    point2DLeftHom(0) = point2DLeft.x;
    point2DLeftHom(1) = point2DLeft.y;
    point2DLeftHom(2) = 1;
    point2DRightHom(0) = point2DRight.x;
    point2DRightHom(1) = point2DRight.y;
    point2DRightHom(2) = 1;
    
    opengv::bearingVector_t bearingVectorLeft, bearingVectorRight;
    bearingVectorLeft = CM1Inv*point2DLeftHom;
    bearingVectorRight = CM2Inv*point2DRightHom;
    
    opengv::bearingVectors_t bearingVectorsLeft, bearingVectorsRight;
    bearingVectorsLeft.push_back(bearingVectorLeft);
    bearingVectorsRight.push_back(bearingVectorRight);
    opengv::relative_pose::CentralRelativeAdapter adapterTriangulation(bearingVectorsLeft, bearingVectorsRight, trasl12, rotMat12);
    opengv::point_t pointTriangulate;
    std::vector<opengv::point_t> points3DTriangulate;
    for (int t=0; t<bearingVectorsLeft.size(); t++)
    {
        opengv::point_t pointTriangulate = opengv::triangulation::triangulate(adapterTriangulation, t);
        points3DTriangulate.push_back(pointTriangulate);
    }
    
    cv::Mat punto(4,1, CV_64F);
    for (int p =0; p<3; p++){
        punto.at<double>(p,0) = points3DTriangulate[0](p);
    }
    punto.at<double>(3,0) = 1;
    
    cv::Mat punti3DRiprLeft = projMat1*punto;
    cv::Point2f punto3D2DLeft(punti3DRiprLeft.at<double>(0,0)/punti3DRiprLeft.at<double>(2,0), punti3DRiprLeft.at<double>(1,0)/punti3DRiprLeft.at<double>(2,0));
    cv::Mat punti3DRiprRight = projMat2*punto;
    cv::Point2f punto3D2DRight(punti3DRiprRight.at<double>(0,0)/punti3DRiprRight.at<double>(2,0), punti3DRiprRight.at<double>(1,0)/punti3DRiprRight.at<double>(2,0));
    
    cv::circle(ImL3, point2DLeft, 20, cv::Scalar_<uchar>(0,0,255));
    cv::circle(ImR3, point2DRight, 20, cv::Scalar_<uchar>(0,0,255));
    cv::circle(ImL3, punto3D2DLeft, 20, cv::Scalar_<uchar>(0,255,0));
    cv::circle(ImR3, punto3D2DRight, 20, cv::Scalar_<uchar>(0,255,0));
    
    cv::imwrite(outputImgPath + "left_reproj_wedge.png", ImL3);
    cv::imwrite(outputImgPath + "right_reproj_wedge.png", ImR3);
}
 */


