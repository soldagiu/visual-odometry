#include <dirent.h>
#include <iostream>
#include "visual_odometry_debug.h"
#include <vector>

using namespace std;
using namespace cv;
using namespace opengv;

int main( int argc, char** argv )
{
    
    // read the paths
    std::string leftDirPath, rightDirPath, camParametersPath, outputImgPath;
    cv::FileStorage f;
    f.open("/Users/giuliasolda/Desktop/VO_software/parameters/paths.xml", cv::FileStorage::READ);
    f["leftDir"]        >> leftDirPath;             // left images directory
    f["rightDir"]       >> rightDirPath;            // right images directory
    f["camParameters"]  >> camParametersPath;       // camera parameters directory
    f["outputImgDir"]   >> outputImgPath;           // output images (debug) directory
    f.release();

    DIR* left;
    DIR* right;
    left = opendir(leftDirPath.c_str());
    right = opendir(rightDirPath.c_str());
    
    vector <string> leftFileName;
    vector <string> rightFileName;
    
    dirent * l;
    dirent * r;
    
    while((l = readdir(left)))
    {
        leftFileName.push_back(l->d_name);
    }
    
    while((r = readdir(right)))
    {
        rightFileName.push_back(r->d_name);
    }
    

    
    // define the class
    visualOdometry prova(visualOdometry::SURF_FEATURE);
    
    prova.cameraParameters(camParametersPath);
    

    
    // Rt initialization
    opengv::transformation_t Rt;      

//    cv::SIFT featuresSiftBlu;
//    cv::Mat descriptorBlu;
//    std::vector<cv::KeyPoint> keypointsBlu;
//    cv::SiftDescriptorExtractor descriptorSiftBlu;
    
//    Mat bluImg = imread("/Users/giuliasolda/Desktop/blu.jpg", 0);
//    featuresSiftBlu.detect(bluImg, keypointsBlu);
//    descriptorSiftBlu.compute(bluImg, keypointsBlu, descriptorBlu);
//    drawKeypoints(bluImg, keypointsBlu, bluImg, Scalar::all(-1), cv::DrawMatchesFlags::DRAW_RICH_KEYPOINTS);
//    cv::imwrite("/Users/giuliasolda/Desktop/bluKeypoint.jpg", bluImg);
    
    int step = 20;
    for (int i=0; i < rightFileName.size()-2; i+=step)
    {
        string leftImgPath = leftDirPath + leftFileName[i+2];
        string rightImgPath = rightDirPath + rightFileName[i+2];
        
        // load two images
        Mat ImL = imread(leftImgPath, 0);
        Mat ImR = imread(rightImgPath, 0);
        
        // add the two images (put 1 for demosaicing+undistortion)
        prova.addLeftImg(ImL, 1);
        prova.addRightImg(ImR, 1);
        
        // compute Rt transformation
        prova.computeRt(Rt, visualOdometry::RANSAC_KNEIP);
 
    }
    
    return 0;
}

