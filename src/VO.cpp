#include <dirent.h>
#include <iostream>

#define VO_WITH_NONFREE         // nonfree vecchio
//#define VO_WITH_CUDA          // CUDA nuovo
//#define VO_WITH_XFATURES2D    // nonfree nuovo

#include "visual_odometry.h"
#include <vector>


using namespace std;
using namespace cv;
using namespace opengv;

std::string leftDirPath, rightDirPath, camParametersPath, outputImgPath, outputTxtPath;
opengv::transformations_t Rt_tot;

void RtWriteTxt(opengv::transformations_t & Rt, std::string opt, std::string outputTxt)
{
    std::stringstream ss;
    ss.str("");
    ss << opt;
    std::ofstream myfile_write_2;
    myfile_write_2.open (outputTxt + ss.str() +".txt");
    myfile_write_2 << "[";
    for(int i=0; i<Rt.size(); i++)
    {
        opengv::transformation_t stampa = Rt[i];
        myfile_write_2 << "{[";
        for (int r=0; r<3; r++)
        {
            myfile_write_2 << "[" <<stampa(r,0) << "," << stampa(r,1) << "," << stampa(r,2) << "," << stampa(r,3) << "]";
            if (r<2)
                myfile_write_2 << ",";
        }
        myfile_write_2 << "]}";
        if (i<Rt.size()-1)
            myfile_write_2 << ",";
    }
    myfile_write_2 << "]";
    myfile_write_2.close();
}


int main( int argc, char** argv )
{
    
    // read the paths
    cv::FileStorage f;
    f.open("/Users/giuliasolda/Desktop/VO_software/parameters/paths.xml", cv::FileStorage::READ);
    f["leftDir"]        >> leftDirPath;             // left images directory
    f["rightDir"]       >> rightDirPath;            // right images directory
    f["camParameters"]  >> camParametersPath;       // camera parameters directory
    f["outputImgDir"]   >> outputImgPath;           // output images (debug) directory
    f["outputTxtDir"]   >> outputTxtPath;           // output txt files (debug) directory
    f.release();

    DIR* left;
    DIR* right;
    left = opendir(leftDirPath.c_str());
    right = opendir(rightDirPath.c_str());
    
    vector <string> leftFileName;
    vector <string> rightFileName;
    
    dirent * l;
    dirent * r;
    
    while((l = readdir(left)))
    {
        leftFileName.push_back(l->d_name);
    }
    
    while((r = readdir(right)))
    {
        rightFileName.push_back(r->d_name);
    }
    
    // sort images
    std::sort(rightFileName.begin(), rightFileName.end());
    std::sort(leftFileName.begin(), leftFileName.end());

    // define the class
    visualOdometry pose(visualOdometry::SIFT_FEATURE);
    
    pose.cameraParameters(camParametersPath);

    // Rt initialization
    opengv::transformation_t Rt;
    
    int step = 30;
    for (int i=0; i < rightFileName.size()-2; i+=step)
    {
        string leftImgPath = leftDirPath + leftFileName[i+2];
        string rightImgPath = rightDirPath + rightFileName[i+2];
        
        auto start_tot = std::chrono::system_clock::now();
        
        // load two images
        Mat ImL = imread(leftImgPath, 0);
        Mat ImR = imread(rightImgPath, 0);
        
        std::cout << leftImgPath << std::endl << rightImgPath << std::endl << std::endl;
        
//         demosaicing
//         the input image must have 1 channel, while the output one must have 3
        cv::Mat ImL3, ImR3;
        cvtColor(ImL, ImL3, CV_BayerGR2GRAY);
        cvtColor(ImR, ImR3, CV_BayerGR2GRAY);
//        cvtColor(ImL, ImL3, CV_BayerGR2BGR);
//        cvtColor(ImR, ImR3, CV_BayerGR2BGR);
        // flip images
        flip(ImL3, ImL3, -1);
        flip(ImR3, ImR3, -1);
        
        // add the two images (put 1 for undistortion)
        pose.addLeftImg(ImL3, 1);
        pose.addRightImg(ImR3, 1);

//        // add the two images (put 1 for undistortion)
//        pose.addLeftImg(ImL, 0);
//        pose.addRightImg(ImR, 0);
        
        auto start = std::chrono::system_clock::now();
        // compute Rt transformation
        pose.computeRt(Rt, visualOdometry::RANSAC_EPNP, 1);
        
        if (i>0)
            Rt_tot.push_back(Rt);
    
        // print the results
        std::cout << "Current pose: " << std::endl << Rt << std::endl << std::endl;
        
        auto end = std::chrono::system_clock::now();
        std::chrono::duration<double> elapsed = end-start;
        std::cout << "Rt computation time: " << elapsed.count() << " s" << std::endl;
        
        std::chrono::duration<double> elapsed_tot = end-start_tot;
        std::cout << "Total time needed: " << elapsed_tot.count() << " s" << std::endl << std::endl;

    }

    RtWriteTxt(Rt_tot, "Epnp_15_deg", outputTxtPath);
    
    return 0;
}


